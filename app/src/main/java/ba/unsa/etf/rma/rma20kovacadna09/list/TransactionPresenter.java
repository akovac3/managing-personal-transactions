package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;
import ba.unsa.etf.rma.rma20kovacadna09.detail.TransactionDetailInteractor;

public class TransactionPresenter implements ITransactionPresenter, ITypeView, TransactionInteractor.OnTransactionsSearchDone, TransactionDetailInteractor.OnDADone {
    private ITransactionView view;
    private Context context;
    private TransactionDBInteractor dbInteractor;
    private ArrayList<Transaction> transactions;
    private ArrayList<Type> types;
    private String[] strings;
    private ITypesPresenter typesPresenter;

    private int order, month, year, type;

    private boolean isConnected() {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    public ITypesPresenter getTypesPresenter(){
        if (typesPresenter == null) {
            typesPresenter = new TypePresenter(this, context);
        }
        return typesPresenter;
    }

    public TransactionPresenter(ITransactionView view, Context context) {
        this.view = view;
        this.context = context;
        transactions = new ArrayList<>();
        types = new ArrayList<>();
        types.addAll(getTypesPresenter().getTypes());
        dbInteractor = new TransactionDBInteractor(context, types);
    }


    @Override
    public void getTransactions(int order, int month, int year, int type) {
        if (isConnected()) {
            this.order = order;
            this.month = month;
            this.year = year;
            this.type = type;
            String sort = "";
            if (order == 0) {
                sort = "amount.asc";
            } else if (order == 1) {
                sort = "amount.desc";
            } else if (order == 2) {
                sort = "title.asc";
            } else if (order == 3) {
                sort = "title.desc";
            } else if (order == 4) {
                sort = "date.asc";
            } else if (order == 5) {
                sort = "date.desc";
            }
            strings = new String[]{
                    String.valueOf(1),
                    context.getString(R.string.api_id),
                    sort,
                    String.valueOf(month),
                    String.valueOf(year),
                    String.valueOf(type)
            };
            new TransactionInteractor(this, types).execute(strings);
        } else {
            transactions = dbInteractor.getTransactionsMonthly(month, year);
            sortTransactions(order);
            view.setTransactions(transactions);
            view.notifyTransactionListDataSetChanged();
        }
    }


    @Override
    public void sortTransactions(int order) {
        if (order == 0) {
            Collections.sort(transactions, new Comparator<Transaction>() {
                public int compare(Transaction t1, Transaction t2) {
                    return (int) (t1.getAmount() - t2.getAmount());
                }
            });
        } else if (order == 1) {
            Collections.sort(transactions, new Comparator<Transaction>() {
                public int compare(Transaction t1, Transaction t2) {
                    return (int) (t2.getAmount() - t1.getAmount());
                }
            });
        } else if (order == 2) {
            Collections.sort(transactions, new Comparator<Transaction>() {
                public int compare(Transaction t1, Transaction t2) {
                    return t1.getTitle().compareTo(t2.getTitle());
                }
            });
        } else if (order == 3) {
            Collections.sort(transactions, new Comparator<Transaction>() {
                public int compare(Transaction t1, Transaction t2) {
                    return t2.getTitle().compareTo(t1.getTitle());
                }
            });
        } else if (order == 4) {
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction t1, Transaction t2) {
                    return t1.getDate().getDayOfMonth() - (t2.getDate().getDayOfMonth());
                }
            });
        } else if (order == 5) {
            Collections.sort(transactions, new Comparator<Transaction>() {
                @Override
                public int compare(Transaction t1, Transaction t2) {
                    return t2.getDate().getDayOfMonth() - (t1.getDate().getDayOfMonth());
                }
            });
        }
    }

    @Override
    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }


    @Override
    public void searchAllTransactions() {
        if (isConnected()) {
            strings = new String[]{
                    String.valueOf(0),
                    context.getString(R.string.api_id)
            };

            new TransactionInteractor(this, types).execute(strings);
        }
        else{
            transactions = dbInteractor.getAllTransactions();
            sortTransactions(order);
            view.setTransactions(transactions);
            view.notifyTransactionListDataSetChanged();
        }
    }

    @Override
    public void onDone(ArrayList<Transaction> results) {
        transactions = results;
        sortTransactions(order);
        view.setTransactions(transactions);
        view.notifyTransactionListDataSetChanged();
    }

    @Override
    public void setTypes(ArrayList<Type> types) {
        this.types = types;
    }

    @Override
    public ArrayList<Transaction> getDBTransactions() {
        return dbInteractor.getAllTransactions();
    }

    @Override
    public void deleteTransactions() {
        dbInteractor.deleteAll();
    }

    @Override
    public void onDone(int result) {
    }

    @Override
    public void set() {
    }
}

