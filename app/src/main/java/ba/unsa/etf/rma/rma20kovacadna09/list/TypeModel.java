package ba.unsa.etf.rma.rma20kovacadna09.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class TypeModel {
    public static ArrayList<Type> types = new ArrayList<>();

    public void napuniModel(ArrayList<Type> tipovi){
        types = new ArrayList<>();
        types.addAll(tipovi);
    }

    public ArrayList<Type> getTypes(){
        return types;
    }
}
