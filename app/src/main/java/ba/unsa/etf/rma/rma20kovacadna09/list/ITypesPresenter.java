package ba.unsa.etf.rma.rma20kovacadna09.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public interface ITypesPresenter {
    void searchTypes();
    ArrayList<Type> getTypes();
}
