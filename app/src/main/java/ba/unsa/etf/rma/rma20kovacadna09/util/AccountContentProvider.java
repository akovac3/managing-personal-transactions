package ba.unsa.etf.rma.rma20kovacadna09.util;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

public class AccountContentProvider extends ContentProvider {

    private static final int ACCOUNTID =1;
    private static final int ALL =2;
    private static final UriMatcher uM;

    static {
        uM = new UriMatcher(UriMatcher.NO_MATCH);
        uM.addURI("rma.provider.account","elements/#",ACCOUNTID);
        uM.addURI("rma.provider.account", "elements", ALL);
    }

    static TransactionDBOpenHelper mHelper;

    @Override
    public boolean onCreate() {
        mHelper = new TransactionDBOpenHelper(getContext(),
                TransactionDBOpenHelper.DATABASE_NAME,null,
                TransactionDBOpenHelper.DATABASE_VERSION);
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArg, @Nullable String sortOrder) {
        SQLiteDatabase database;
        try{
            database=mHelper.getWritableDatabase();
        }catch (SQLiteException e){
            database=mHelper.getReadableDatabase();
        }
        String groupby=null;
        String having=null;
        SQLiteQueryBuilder squery = new SQLiteQueryBuilder();

        switch (uM.match(uri)){
            case ACCOUNTID:
                String idRow = uri.getPathSegments().get(1);
                squery.appendWhere(TransactionDBOpenHelper.ACCOUNT_ID+"="+idRow);
            default:break;
        }
        squery.setTables(TransactionDBOpenHelper.ACCOUNT_TABLE);
        Cursor cursor = squery.query(database,projection,selection,selectionArg,groupby,having,sortOrder);
        return cursor;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        switch (uM.match(uri)){
            case ACCOUNTID:
                return "vnd.android.cursor.dir/vnd.rma.elemental";
            case ALL:
                return "vnd.android.cursor.item/vnd.rma.elemental";
            default:
                throw new IllegalArgumentException("Unsupported uri: "+uri.toString());
        }
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues contentValues) {
        SQLiteDatabase database;
        try{
            database=mHelper.getWritableDatabase();
        }catch (SQLiteException e){
            database=mHelper.getReadableDatabase();
        }
        long id = database.insert(TransactionDBOpenHelper.ACCOUNT_TABLE, null, contentValues);
        return uri.buildUpon().appendPath(String.valueOf(id)).build();
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String s, @Nullable String[] strings) {
        int count = 0;
        SQLiteDatabase database;
        try{
            database=mHelper.getWritableDatabase();
        }catch (SQLiteException e){
            database=mHelper.getReadableDatabase();
        }
        switch (uM.match(uri)){
            case ACCOUNTID:
                String idRow = uri.getPathSegments().get(1);
                String where = TransactionDBOpenHelper.ACCOUNT_ID+"="+idRow;
                count = database.delete(TransactionDBOpenHelper.ACCOUNT_TABLE,where, null);
                break;
            case ALL:
                database.delete(TransactionDBOpenHelper.ACCOUNT_TABLE,null, null);
            default:break;
        }

        return count;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues contentValues, @Nullable String s, @Nullable String[] strings) {
        int count = 0;
        SQLiteDatabase database;
        try{
            database=mHelper.getWritableDatabase();
        }catch (SQLiteException e){
            database=mHelper.getReadableDatabase();
        }
        switch (uM.match(uri)){
            case ACCOUNTID:
                String idRow = uri.getPathSegments().get(1);
                String where = TransactionDBOpenHelper.ACCOUNT_ID+"="+idRow;
                count = database.update(TransactionDBOpenHelper.ACCOUNT_TABLE, contentValues, where, null);
                break;
            default:break;
        }

        return count;
    }

}
