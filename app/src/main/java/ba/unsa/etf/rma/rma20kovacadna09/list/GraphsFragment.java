package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Calendar;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;



public class GraphsFragment extends Fragment implements ITransactionView, ITypeView {
    private RadioGroup radioGroup;
    private RadioButton radioMonth;
    private RadioButton radioDay;
    private  RadioButton radioWeek;
    private BarChart paymentChart;
    private BarChart incomeChart;
    private BarChart allChart;

    private ArrayList<BarEntry> barEntriesPayment;
    private ArrayList<BarEntry> barEntriesIncome;
    private ArrayList<BarEntry> barEntriesAll;
    private ArrayList<Integer> labels;
    private ITypesPresenter typesPresenter;

    public ITypesPresenter getTypesPresenter(){
        if (typesPresenter == null) {
            typesPresenter = new TypePresenter(this, getActivity());
        }
        return typesPresenter;
    }


    private ITransactionPresenter presenter;

    private ITransactionPresenter getPresenter() {
        if(presenter == null){
            presenter = new TransactionPresenter(this, getActivity());
        }
        return presenter;
    }


    private ArrayList<Transaction> transactions;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.graph_fragment, container, false);

        radioGroup = view.findViewById(R.id.radioGroup);
        radioDay = view.findViewById(R.id.daily);
        radioMonth = view.findViewById(R.id.monthly);
        radioWeek = view.findViewById(R.id.weekly);
        paymentChart = view.findViewById(R.id.paymentChart);
        incomeChart = view.findViewById(R.id.incomeChart);
        allChart = view.findViewById(R.id.allChart);

        radioWeek.setOnClickListener(listener);
        radioMonth.setOnClickListener(monthListener);
        radioDay.setOnClickListener(dayListener);

        barEntriesPayment = new ArrayList<>();
        barEntriesAll = new ArrayList<>();
        barEntriesIncome = new ArrayList<>();
        labels = new ArrayList<>();
        ArrayList<Type> types = new ArrayList<>(getTypesPresenter().getTypes());
        getPresenter().setTypes(types);
        getPresenter().searchAllTransactions();
        return view;
    }

    private void getDaily() {
        ArrayList<Integer> lab = new ArrayList<>();
        for(int i=1; i<= LocalDate.now().getMonth().maxLength();i++){
            lab.add(i);
        }
        labels.clear();
        labels = lab;
        getPaymentTransactionsDaily();
        getIncomeTransactionsDaily();
        getAllTransactionsDaily();

        makeChart(barEntriesPayment, "Potrosnja", "#FA8072", "Days", paymentChart);
        makeChart(barEntriesIncome, "Zarada", "#9CFA75", "Days", incomeChart);
        makeChart(barEntriesAll, "Ukupno stanje", "#F4FA75", "Days", allChart);
    }

    private void getWeekly() {
        ArrayList<Integer> lab = new ArrayList<>();
        for(int i=1; i<= 5;i++){
            lab.add(i);
        }
        labels.clear();
        labels = lab;
        getPaymentTransactionsWeekly();
        getIncomeTransactionsWeekly();
        getAllTransactionsWeekly();

        makeChart(barEntriesPayment, "Potrosnja", "#FA8072", "Weeks", paymentChart);
        makeChart(barEntriesIncome, "Zarada", "#9CFA75", "Weeks", incomeChart);
        makeChart(barEntriesAll, "Ukupno stanje", "#F4FA75", "Weeks", allChart);
    }


    private void getMonthly(){
        ArrayList<Integer> lab = new ArrayList<>();
        for(int i=1;i<=12;i++){
            lab.add(i);
        }
        labels.clear();
        labels = lab;
        getPaymentTransactionsMonthly();
        getIncomeTransactionsMonthly();
        getAllTransactionsMonthly();

        makeChart(barEntriesPayment, "Potrosnja", "#FA8072", "Months", paymentChart);
        makeChart(barEntriesIncome, "Zarada", "#9CFA75", "Months", incomeChart);
        makeChart(barEntriesAll, "Ukupno stanje", "#F4FA75", "Months", allChart);

    }

    private double getRegular(Transaction t, int day, int month){
        double result =0;
        month--;
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.YEAR, t.getDate().getYear());
        ca.set(Calendar.MONTH, t.getDate().getMonthValue() - 1);
        ca.set(Calendar.DAY_OF_MONTH, t.getDate().getDayOfMonth());
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, t.getEndDate().getYear());
        c.set(Calendar.MONTH, t.getEndDate().getMonthValue() - 1);
        c.set(Calendar.DAY_OF_MONTH, t.getEndDate().getDayOfMonth());

        while (!ca.after(c) ) {
            if((day==0 && ca.get(Calendar.MONTH)==month) || (ca.get(Calendar.MONTH)==month && ca.get(Calendar.DAY_OF_MONTH)==day)){
                result+=t.getAmount();
            }

            ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
        }
        return result;
    }



    private void makeChart(ArrayList<BarEntry> entries, String label, String color, String desc, BarChart bc ){
        BarDataSet barDataSet = new BarDataSet(entries, label);
        barDataSet.setColors(ColorTemplate.rgb(color));
        Description description = new Description();
        description.setText(desc);
        bc.setDescription(description);
        BarData barData = new BarData(barDataSet);
        bc.setData(barData);

        XAxis xAxis = bc.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setDrawAxisLine(false);
        xAxis.setGranularity(1f);
        xAxis.setLabelCount(labels.size());
        xAxis.setLabelRotationAngle(0);
        bc.animateY(2000);
        bc.invalidate();

    }

    private void getIncomeTransactionsMonthly(){
        barEntriesIncome.clear();
        LocalDate ld = LocalDate.now();
        for(int i =1; i<13;i++) {
            double r=0;
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Individual income") || t.getType().getName().equals("Regular income")) {

                    if(t.getType().getName().equals("Regular income"))
                        r += getRegular(t, 0, i);
                    else if(t.getDate().getMonthValue() == i)
                        r += t.getAmount();
                }
            }
            barEntriesIncome.add(new BarEntry(i, (float) r));
        }
    }

    private void getAllTransactionsMonthly(){
        barEntriesAll.clear();
        LocalDate ld = LocalDate.now();
        double r=0;
        for(int i =1; i<13;i++) {
            for (Transaction t : transactions) {
                if(t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Regular payment"))
                    r -= getRegular(t, 0, i);
                else if(t.getDate().getYear()== ld.getYear() && (t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Purchase")) && t.getDate().getMonthValue() == i)
                    r -=t.getAmount();
                else if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Regular income"))
                    r +=getRegular(t, 0, i);
                else if(t.getDate().getYear()== ld.getYear() && t.getDate().getMonthValue() == i)
                    r += t.getAmount();
            }
            barEntriesAll.add(new BarEntry(i, (float) r));
        }
    }

    private void getPaymentTransactionsMonthly(){
        barEntriesPayment.clear();
        LocalDate ld = LocalDate.now();
        for(int i =1; i<13;i++) {
            double r=0;
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Regular payment") || t.getType().getName().equals("Purchase")) {
                    if(t.getType().getName().equals("Regular payment"))
                        r += getRegular(t, 0, i);
                    else if(t.getDate().getMonthValue() == i)
                        r += t.getAmount();
                }
            }
            barEntriesPayment.add(new BarEntry(i, (float) r));
        }
    };

    private void getPaymentTransactionsDaily(){
        barEntriesPayment.clear();
        LocalDate ld = LocalDate.now();
        int b = ld.getMonth().maxLength();
        for(int i = 1; i<=b;i++){
            double r=0;
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Regular payment") || t.getType().getName().equals("Purchase")) {
                    if(t.getType().getName().equals("Regular payment"))
                        r += getRegular(t, i, ld.getMonthValue());
                    else if(t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().getDayOfMonth() == i)
                        r += t.getAmount();
                }
            }
            barEntriesPayment.add(new BarEntry(i, (float) r));
        }
    }

    private void getIncomeTransactionsDaily(){
        barEntriesIncome.clear();
        LocalDate ld = LocalDate.now();
        int b = ld.getMonth().maxLength();
        for(int i = 1; i<=b;i++){
            double r=0;
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Individual income") || t.getType().getName().equals("Regular income")) {
                    if(t.getType().getName().equals("Regular income"))
                        r += getRegular(t, i, ld.getMonthValue());
                    else if(t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().getDayOfMonth() == i)
                        r += t.getAmount();
                }
            }
            barEntriesIncome.add(new BarEntry(i, (float) r));
        }
    }

    private void getAllTransactionsDaily(){
        barEntriesAll.clear();
        LocalDate ld = LocalDate.now();
        int b = ld.getMonth().maxLength();
        double r=0;
        for(int i = 1; i<=b;i++){
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && (t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Purchase")) && t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().getDayOfMonth() == i)
                    r-= t.getAmount();
                else if(t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Regular payment"))
                    r -= getRegular(t, i, ld.getMonthValue());
                else if(t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Regular income"))
                    r += getRegular(t, i, ld.getMonthValue());
                else if(t.getDate().getYear()== ld.getYear() && t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().getDayOfMonth() == i)
                    r += t.getAmount();
            }
            barEntriesAll.add(new BarEntry(i, (float) r));
        }
    }

    private double getRegularWeekly(Transaction t, int week, int month){
        double result =0;
        LocalDate ld = t.getDate();
        while (ld.isBefore(t.getEndDate()) ) {

            if(ld.getMonthValue()==month && ld.get(ChronoField.ALIGNED_WEEK_OF_MONTH)==week){
                result+=t.getAmount();
            }
            ld= ld.plusDays(t.getTransactionInterval());
        }
        return result;
    }
    private void getPaymentTransactionsWeekly(){
        barEntriesPayment.clear();
        LocalDate ld = LocalDate.now();
        for(int i = 1; i<=5;i++){
            double r=0;
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Regular payment") || t.getType().getName().equals("Purchase")) {
                    if(t.getType().getName().equals("Regular payment"))
                        r += getRegularWeekly(t, i, ld.getMonthValue());
                    else if(t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().get(ChronoField.ALIGNED_WEEK_OF_MONTH) == i)
                        r += t.getAmount();
                }
            }
            barEntriesPayment.add(new BarEntry(i, (float) r));
        }
    }

    private void getIncomeTransactionsWeekly(){
        barEntriesIncome.clear();
        LocalDate ld = LocalDate.now();
        for(int i = 1; i<=5;i++){
            double r=0;
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Individual income") || t.getType().getName().equals("Regular income")) {
                    if(t.getType().getName().equals("Regular income"))
                        r += getRegularWeekly(t, i, ld.getMonthValue());
                    else if(t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().get(ChronoField.ALIGNED_WEEK_OF_MONTH) == i)
                        r += t.getAmount();
                }
            }
            barEntriesIncome.add(new BarEntry(i, (float) r));
        }
    }

    private void getAllTransactionsWeekly(){
        barEntriesAll.clear();
        LocalDate ld = LocalDate.now();
        double r=0;
        for(int i = 1; i<=5;i++){
            for (Transaction t : transactions) {
                if (t.getDate().getYear()== ld.getYear() && (t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Purchase")) && t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().get(ChronoField.ALIGNED_WEEK_OF_MONTH) == i)
                    r-= t.getAmount();
                else if(t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Regular payment"))
                    r -= getRegularWeekly(t, i, ld.getMonthValue());
                else if(t.getDate().getYear()== ld.getYear() && t.getType().getName().equals("Regular income"))
                    r += getRegularWeekly(t, i, ld.getMonthValue());
                else if(t.getDate().getYear()== ld.getYear() && t.getDate().getMonthValue() == ld.getMonthValue() && t.getDate().get(ChronoField.ALIGNED_WEEK_OF_MONTH) == i)
                    r += t.getAmount();
            }
            barEntriesAll.add(new BarEntry(i, (float) r));
        }
    }



    private View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getWeekly();
        }
    };

    private View.OnClickListener monthListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getMonthly();
        }
    };
    private View.OnClickListener dayListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getDaily();
        }
    };

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        this.transactions = transactions;
        getMonthly();
    }

    @Override
    public void notifyTransactionListDataSetChanged() {

    }


    @Override
    public void onResume() {
        super.onResume();
        radioMonth.setChecked(true);
    }

    @Override
    public void set() {

    }
}