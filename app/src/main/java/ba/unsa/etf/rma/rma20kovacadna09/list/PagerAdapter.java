package ba.unsa.etf.rma.rma20kovacadna09.list;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import ba.unsa.etf.rma.rma20kovacadna09.account.BudgetFragment;

public class PagerAdapter extends FragmentStatePagerAdapter {

    public PagerAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        Fragment fragment = null;
        switch (position){
            case 1 :
                fragment= new TransactionListFragment();
                break;
            case 2:
                fragment = new BudgetFragment();
                break;
            case 3:
                fragment = new GraphsFragment();
                break;
            default:
                fragment = new Fragment();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 5;
    }
}
