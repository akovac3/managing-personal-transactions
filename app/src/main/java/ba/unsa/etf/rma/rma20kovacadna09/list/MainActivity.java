package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.account.AccountPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.account.IAccountPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.account.IAccountView;
import ba.unsa.etf.rma.rma20kovacadna09.data.Account;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.detail.ITransactionDetailPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.detail.ITransactionDetailView;
import ba.unsa.etf.rma.rma20kovacadna09.detail.TransactionDetailFragment;
import ba.unsa.etf.rma.rma20kovacadna09.detail.TransactionDetailPresenter;

public class MainActivity extends AppCompatActivity implements ITransactionView, ITransactionDetailView, IAccountView, TransactionListFragment.OnItemClick, TransactionDetailFragment.OnSaveClick, ITypeView{
    private boolean landscapeMode;
    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
    private int prva = 1;
    private ITransactionPresenter presenter;
    private IAccountPresenter accountPresenter;
    private ITransactionDetailPresenter detailPresenter;
    private Account account;
    private ITypesPresenter typesPresenter;

    public ITypesPresenter getTypesPresenter(){
        if (typesPresenter == null) {
            typesPresenter = new TypePresenter(this, this);
        }
        return typesPresenter;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getTypesPresenter().searchTypes();
        if(isConnected()) {
            getAccountPresenter().deleteAccount();
        } else prva = 2;
        getAccountPresenter().searchAccounts(getString(R.string.api_id));
    }

    public ITransactionDetailPresenter getDetailPresenter() {
        if (detailPresenter == null) {
            detailPresenter = new TransactionDetailPresenter( this, this);
        }
        return detailPresenter;
    }

    public ITransactionPresenter getPresenter() {
        if (presenter == null) {
            presenter = new TransactionPresenter(this, this);
        }
        return presenter;
    }

    public IAccountPresenter getAccountPresenter(){
        if(accountPresenter == null) {
            accountPresenter = new AccountPresenter(this, this);
        }
        return accountPresenter;
    }

    private BroadcastReceiver br = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

            if (cm.getActiveNetworkInfo() == null) {
                Toast toast = Toast.makeText(context, "Disconnected", Toast.LENGTH_SHORT);
                prva =2;
                toast.show();

            } else {
                Toast toast = Toast.makeText(context, "Ažuriranje", Toast.LENGTH_LONG);
                ArrayList<Transaction> transactions = getPresenter().getDBTransactions();
                saveAccount();

                for (Transaction t : transactions) {
                    if(t.getDeleted()==2) {
                        getDetailPresenter().updateTransaction(t);
                    }
                    else if(t.getDeleted()==3){
                        getDetailPresenter().removeTransaction(t.getId(), 0);
                    }
                    else {
                        getDetailPresenter().addTransaction(t);
                    }
                }
                getPresenter().deleteTransactions();
                Intent intent1 = new Intent("ba.unsa.rma.ACTION");
                sendBroadcast(intent1);
                toast.show();
            }
        }
    };

    private void saveAccount() {
        if(prva!=1) {
            Account tr = getAccountPresenter().getDBAccount();
            getAccountPresenter().changeAccount(28, null, String.valueOf(tr.getMonthLimit()), String.valueOf(tr.getTotalLimit()));
        }
    }

    private boolean isConnected() {
        return ((ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    @Override
    public void onPause() {
        unregisterReceiver(br);
        super.onPause();
    }

    @Override
    public void onItemClicked(Transaction transaction) {
        Bundle arguments = new Bundle();
        arguments.putParcelable("transaction", transaction);
        TransactionDetailFragment detailFragment = new TransactionDetailFragment();
        detailFragment.setArguments(arguments);
        if (landscapeMode) {
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_detail, detailFragment).commit();
        } else {
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_list, detailFragment).addToBackStack(null).commit();
        }
    }

    @Override
    public void onSaveClicked(Transaction newTransaction) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        TransactionListFragment listFragment = null;
        if (landscapeMode) {
            listFragment = (TransactionListFragment) fragmentManager.findFragmentById(R.id.transaction_list);
        } else if (newTransaction == null) {
            fragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }
        if (listFragment != null)
            listFragment.refresh(newTransaction);

        if (landscapeMode && newTransaction==null) {
            Bundle arguments = new Bundle();
            arguments.putParcelable("transaction", newTransaction);
            TransactionDetailFragment detailFragment = new TransactionDetailFragment();
            detailFragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction().replace(R.id.transactions_detail, detailFragment).commit();
        }
    }

    @Override
    public void set() {
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FrameLayout details = findViewById(R.id.transactions_detail);
        if (details != null) {
            Bundle bundle = new Bundle();
            bundle.putParcelable("transaction", null);
            landscapeMode = true;

            TransactionDetailFragment detailFragment = new TransactionDetailFragment();
            detailFragment.setArguments(bundle);
            fragmentManager.beginTransaction().replace(R.id.transactions_detail, detailFragment).commit();
        } else {
            landscapeMode = false;
        }
        Fragment listFragment;
        if (landscapeMode) {
            listFragment = new TransactionListFragment();
            fragmentManager.beginTransaction().replace(R.id.transaction_list, listFragment).commit();
        } else {
            listFragment = new PagerFragment();
            fragmentManager.beginTransaction().replace(R.id.frame_list, listFragment).commit();
        }
        registerReceiver(br, filter);
    }

    @Override
    public void refreshView() {
        account = getAccountPresenter().getAccount();
        if(prva==1) getAccountPresenter().writeAccountInDB(account);
    }

    @Override
    public void setTransactions(ArrayList<Transaction> transactions) {

    }

    @Override
    public void notifyTransactionListDataSetChanged() {

    }

    @Override
    public void setTransaction(Transaction transaction) {

    }

}
