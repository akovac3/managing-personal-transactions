package ba.unsa.etf.rma.rma20kovacadna09.detail;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Parcelable;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;
import ba.unsa.etf.rma.rma20kovacadna09.list.ITypeView;
import ba.unsa.etf.rma.rma20kovacadna09.list.ITypesPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.list.TransactionDBInteractor;
import ba.unsa.etf.rma.rma20kovacadna09.list.TransactionInteractor;
import ba.unsa.etf.rma.rma20kovacadna09.list.TypePresenter;

public class TransactionDetailPresenter implements ITransactionDetailPresenter, ITypeView, TransactionDetailInteractor.OnDADone, TransactionInteractor.OnTransactionsSearchDone {
    private Context context;
    private Transaction transaction;
    private ArrayList<Transaction> transactions;
    private ITransactionDetailView view;
    private ArrayList<Type> types;
    private ITypesPresenter typesPresenter;
    private TransactionDBInteractor interactor;
    private DetailDBInteractor detailInteractor;

    public ITypesPresenter getTypesPresenter(){
        if (typesPresenter == null) {
            typesPresenter = new TypePresenter(this, context);
        }
        return typesPresenter;
    }


    public TransactionDetailPresenter(ITransactionDetailView view, Context context) {
        this.context = context;
        transactions = new ArrayList<>();
        this.view = view;
        types= new ArrayList<>();
        types.addAll(getTypesPresenter().getTypes());
        interactor = new TransactionDBInteractor(context,types);
        detailInteractor = new DetailDBInteractor(context, types);
        String[] strings = new String[]{
                String.valueOf(0),
                context.getString(R.string.api_id)
        };

        if(isConnected()) new TransactionInteractor(this, types).execute(strings);
        else{
            interactor.getAllTransactions();
        }
    }

    private boolean isConnected() {
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }


    @Override
    public void create(Integer internal, Integer id, LocalDate date, double amount, String title, Type type, String itemDescription, String transactionInterval, LocalDate endDate) {
        Integer interval = null;
        if(transactionInterval!=null)interval = Integer.parseInt(transactionInterval);
        transaction = new Transaction(internal, id, date, amount, title, type, itemDescription, interval, endDate);
    }

    @Override
    public Transaction getTransaction() {
        return transaction;
    }

    @Override
    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public void insertInDB(Transaction transaction){
        int id = detailInteractor.addTransaction(transaction);
        transaction.setInternalId(id);
    }

    @Override
    public double getCurrentLimit(Transaction transaction, boolean all) {
        double result = 0;
        int month = transaction.getDate().getMonthValue();
        for (Transaction t : transactions) {
            if(transaction.getId()!=null && t.getId().equals(transaction.getId())) continue;
            if (!all  && (t.getType().getName().toLowerCase().equals("individual payment") || t.getType().getName().toLowerCase().equals("regular payment") || t.getType().getName().toLowerCase().equals("purchase"))) {
                if (t.getType().getName().toLowerCase().equals("regular payment")){
                    result += getRegular(t, month-1);
                } else if (t.getDate().getMonthValue() == month)
                    result += t.getAmount();
            }
            if (all ) {
                if (t.getType().getName().toLowerCase().equals("individual payment") || t.getType().getName().toLowerCase().equals("regular payment") || t.getType().getName().toLowerCase().equals("purchase")) {
                    if (t.getType().getName().toLowerCase().equals("regular payment")) {
                        result += getRegular(t,0);
                    } else {
                        result += t.getAmount();
                    }
                }
            }
        }
        if (all && transaction.getType().getName().equals("Regular payment")) {
            result += getRegular(transaction, 0);
        } else  {
            if(transaction.getType().getName().equals("Regular payment"))result += getRegular(transaction, transaction.getDate().getMonthValue()-1);
            else result += transaction.getAmount();
        }

        return result;
    }

    private double getRegular(Transaction t, int month) {
        double r = 0;
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.YEAR, t.getDate().getYear());
        ca.set(Calendar.MONTH, t.getDate().getMonthValue() - 1);
        ca.set(Calendar.DAY_OF_MONTH, t.getDate().getDayOfMonth());
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, t.getEndDate().getYear());
        c.set(Calendar.MONTH, t.getEndDate().getMonthValue() - 1);
        c.set(Calendar.DAY_OF_MONTH, t.getEndDate().getDayOfMonth());
        while (!ca.after(c)) {
            if(month == 0 || month ==ca.get(Calendar.MONTH))
            r += t.getAmount();
            ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
        }
        return r;
    }

    @Override
    public void addTransaction(Transaction transaction) {
        if(isConnected()) {
            String interval;
            if (transaction.getTransactionInterval() == null) interval = null;
            else interval = String.valueOf(transaction.getTransactionInterval());
            String end;
            if (transaction.getEndDate() == null) end = null;
            else end = transaction.getEndDate().toString();
            String[] strings = {
                    "1",
                    context.getString(R.string.api_id),
                    transaction.getDate().toString(),
                    transaction.getTitle(),
                    String.valueOf(transaction.getAmount()),
                    end,
                    transaction.getItemDescription(),
                    interval,
                    String.valueOf(transaction.getType().getId())
            };
            new TransactionDetailInteractor(this).execute(strings);
        }
        else{

            if (transaction.getId() == null || transaction.getId()==0) {
                int id = detailInteractor.getId();
                transaction.setId(id);
            }
            else{
                transaction.setDeleted(2);
            }
            int internal = detailInteractor.addTransaction(transaction);
            this.transaction.setInternalId(internal);
        }
    }

    @Override
    public void removeTransaction(int id, int delete) {
        if(isConnected()) {
            String[] strings = {
                    "0",
                    context.getString(R.string.api_id),
                    String.valueOf(id)
            };
            new TransactionDetailInteractor(this).execute(strings);
        }
        else{
            detailInteractor.deleteTransaction(id, delete);
        }
    }

    @Override
    public void setTransaction(Parcelable transaction) {
        this.transaction = (Transaction) transaction;
    }

    @Override
    public void updateTransaction(Transaction nova) {
        if(isConnected()) {
            String interval;
            if (nova.getTransactionInterval() == null) interval = null;
            else interval = String.valueOf(nova.getTransactionInterval());
            String end;
            if (nova.getEndDate() == null) end = null;
            else end = nova.getEndDate().toString();
            String[] strings = {
                    "2",
                    context.getString(R.string.api_id),
                    String.valueOf(nova.getId()),
                    nova.getDate().toString(),
                    nova.getTitle(),
                    String.valueOf(nova.getAmount()),
                    end,
                    nova.getItemDescription(),
                    interval,
                    String.valueOf(nova.getType().getId())
            };
            new TransactionDetailInteractor(this).execute(strings);
        }
        else{
            detailInteractor.updateTransaction(nova);
        }
    }

    @Override
    public Transaction getTransactionById(int id) {
        return detailInteractor.getTransaction(id);
    }


    @Override
    public void onDone(int result) {
        if(transaction!=null) transaction.setId(result);
        view.setTransaction(transaction);
    }

    @Override
    public void onDone(ArrayList<Transaction> results) {
        transactions = results;
    }


    @Override
    public void set() {

    }
}
