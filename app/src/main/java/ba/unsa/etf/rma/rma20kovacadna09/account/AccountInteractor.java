package ba.unsa.etf.rma.rma20kovacadna09.account;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.ConnectivityManager;
import android.net.Uri;

import ba.unsa.etf.rma.rma20kovacadna09.data.Account;
import ba.unsa.etf.rma.rma20kovacadna09.util.TransactionDBOpenHelper;

public class AccountInteractor implements IAccountInteractor, GetAccount.OnAccountSearchDone, ChangeAccount.OnChangeDone {
    private Account account;
    private OnSearchDone caller;
    private Context context;

    public AccountInteractor(Context context, OnSearchDone p) {
        caller = p;
        this.context = context;
    };

    private boolean isConnected(){
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    @Override
    public Account getAccount() {
        return account;
    }

    @Override
    public void searchAccount(String api_id, int id) {
        if(isConnected())
            new GetAccount(this).execute(api_id);
        else{
            ContentResolver cr = context.getApplicationContext().getContentResolver();
            String[] kolone = {
                    TransactionDBOpenHelper.ACCOUNT_ID,
                    TransactionDBOpenHelper.ACCOUNT_BUDGET,
                    TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT,
                    TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT
            };
            Uri adresa = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),id);
            String where = null;
            String whereArgs[] = null;
            String order = null;
            Cursor cursor = null;
            try {
                 cursor = cr.query(adresa, kolone, where, whereArgs, order);
            } catch (Exception e){
                e.printStackTrace();
            }
            if (cursor != null && cursor.moveToFirst()) {
                int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_ID);
                int budgetPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET);
                int tLPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT);
                int mLPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT);
                account = new Account(cursor.getInt(idPos), cursor.getDouble(budgetPos), cursor.getDouble(tLPos), cursor.getDouble(mLPos));
                cursor.close();
            }
            caller.onDone(account);
        }
    }

    @Override
    public void changeAccount(String api_id, int id, String budget, String monthLimit, String totalLimit) {
        if(budget!=null) changeBudget(api_id,id, budget);
        else {
            if(isConnected()) {
                StringBuilder sb = new StringBuilder();
                sb.append("{");
                sb.append("\"monthLimit\":").append(Double.parseDouble(monthLimit));
                sb.append(",").append("\"totalLimit\":").append(Double.parseDouble(totalLimit));
                sb.append("}");
                account = new Account(account.getBudget(), Double.parseDouble(totalLimit), Double.parseDouble(monthLimit));
                new ChangeAccount(this, api_id).execute(sb.toString());
            }
            else {
                ContentResolver cr = context.getApplicationContext().getContentResolver();
                Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),id);
                ContentValues values = new ContentValues();
                values.put(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT, monthLimit);
                values.put(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT, totalLimit);
                account = new Account(account.getBudget(), Double.parseDouble(totalLimit), Double.parseDouble(monthLimit));
                cr.update(uri, values, null, null);
                caller.onDone(account);
            }
        }
    }

    @Override
    public void insertIntoDB(Account account) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.account/elements");
        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.ACCOUNT_ID, account.getId());
        values.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, account.getBudget());
        values.put(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT, account.getTotalLimit());
        values.put(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT, account.getMonthLimit());
        cr.insert(uri,values);

    }

    @Override
    public void deleteAccount() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),28);
        cr.delete(uri, null, null);
    }

    @Override
    public void deleteAllAccount() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.account/elements");
        cr.delete(uri, null, null);
    }

    private void changeBudget(String api_id, int id, String budget) {
        if(isConnected()) {
            StringBuilder sb = new StringBuilder();
            sb.append("{");
            sb.append("\"budget\":").append(Double.parseDouble(budget));
            sb.append("}");
            account = new Account(Double.parseDouble(budget), account.getTotalLimit(), account.getMonthLimit());
            new ChangeAccount(this, api_id).execute(sb.toString());
        }
        else{
            ContentResolver cr = context.getApplicationContext().getContentResolver();
            Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),id);
            ContentValues values = new ContentValues();
            values.put(TransactionDBOpenHelper.ACCOUNT_BUDGET, budget);

            cr.update(uri, values, null, null);
            account = new Account(Double.parseDouble(budget), account.getTotalLimit(), account.getMonthLimit());
            caller.onDone(account);
        }

    }

    @Override
    public void onDone(Account result) {
        account = result;
        caller.onDone(result);
    }

    @Override
    public void onDone() {
        caller.onDone(account);
    }

    public interface OnSearchDone {
        public void onDone(Account results);
    }

}
