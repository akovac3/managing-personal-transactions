package ba.unsa.etf.rma.rma20kovacadna09.account;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Account;
import ba.unsa.etf.rma.rma20kovacadna09.list.GetBudget;
import ba.unsa.etf.rma.rma20kovacadna09.util.TransactionDBOpenHelper;

public class AccountPresenter implements IAccountPresenter, AccountInteractor.OnSearchDone, GetBudget.onBudgetDone {
    private IAccountInteractor interactor;
    private Context context;
    private IAccountView view;
    private Account account;
    private int id;

    public AccountPresenter(IAccountView view, Context context){
        this.context = context;
        interactor = new AccountInteractor(context, this);
        this.view=view;

    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    @Override
    public void changeAccount(int id, String budget, String monthLimit, String totalLimit) {
        interactor.changeAccount(context.getString(R.string.api_id),id, budget, monthLimit, totalLimit);
    }

    @Override
    public void changeBudget(int id) {
        this.id = id;
        new GetBudget(this, context).changeBudget();
    }

    @Override
    public void writeAccountInDB(Account account) {
        interactor.insertIntoDB(account);
    }

    @Override
    public void searchAccounts(String api_id) {
        interactor.searchAccount(api_id, 28);
    }

    @Override
    public void onDone(Account result) {
            account = result;
            view.refreshView();
    }
    public void deleteAccount(){
        interactor.deleteAccount();
    }

    public void deleteAll(){
        interactor.deleteAllAccount();
    }

    @Override
    public Account getDBAccount() {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = {
                TransactionDBOpenHelper.ACCOUNT_ID,
                TransactionDBOpenHelper.ACCOUNT_BUDGET,
                TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT,
                TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT
        };
        Uri adresa = ContentUris.withAppendedId(Uri.parse("content://rma.provider.account/elements"),28);
        String where = null;
        String whereArgs[] = null;
        String order = null;
        Cursor cursor = cr.query(adresa,kolone,where,whereArgs,order);
        Account DBAccount = null;
        if (cursor != null) {
            cursor.moveToFirst();
            int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_ID);
            int budgetPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_BUDGET);
            int tLPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_TOTAL_LIMIT);
            int mLPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.ACCOUNT_MONTH_LIMIT);
            DBAccount = new Account(cursor.getInt(idPos), cursor.getDouble(budgetPos), cursor.getDouble(tLPos), cursor.getDouble(mLPos));
        }
        cursor.close();
        return  DBAccount;
    }

    @Override
    public void onAmountDone(double amount) {
        interactor.changeAccount(context.getString(R.string.api_id),id, String.valueOf(amount), null, null);
    }
}
