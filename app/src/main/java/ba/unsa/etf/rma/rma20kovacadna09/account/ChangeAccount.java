package ba.unsa.etf.rma.rma20kovacadna09.account;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class ChangeAccount extends AsyncTask<String, Integer, Void> {
    private OnChangeDone caller;
    private String apiId;

    public ChangeAccount(OnChangeDone caller, String apiId) {
        this.caller = caller;
        this.apiId = apiId;
    }

    @Override
    protected Void doInBackground(String... strings) {
        String api_id = null;
        try {
            api_id = URLEncoder.encode(apiId, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id ;
        URL url;

        try {


            url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setDoOutput(true);

            try(OutputStream os = urlConnection.getOutputStream()) {
                byte[] bytes = strings[0].getBytes("utf-8");
                os.write(bytes, 0, bytes.length);
            }

            try(BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()))){
                StringBuilder result = new StringBuilder();
                String resultLine;
                while((resultLine=br.readLine())!=null){
                    result.append(resultLine.trim());
                }
                Log.d("RESPONSE", result.toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone();
    }


    public interface OnChangeDone {
        public void onDone();
    }

}
