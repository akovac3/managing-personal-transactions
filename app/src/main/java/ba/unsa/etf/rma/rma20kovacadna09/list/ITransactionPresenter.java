package ba.unsa.etf.rma.rma20kovacadna09.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public interface ITransactionPresenter {
    void sortTransactions(int order);
    ArrayList<Transaction> getTransactions();
    void searchAllTransactions();
    void getTransactions(int order, int month, int year, int type);
    void setTypes(ArrayList<Type> types);
    ArrayList<Transaction> getDBTransactions();
    void deleteTransactions();
}
