package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class TransactionInteractor extends AsyncTask<String, Void, Void> implements ITransactionInteractor{
    private ArrayList<Transaction> transactions;
    private OnTransactionsSearchDone caller;
    private ArrayList<Type> types;


    public TransactionInteractor(OnTransactionsSearchDone p, ArrayList<Type> types) {
        caller = p;
        transactions = new ArrayList<>();
        this.types = types;
    }

    public interface OnTransactionsSearchDone {
        public void onDone(ArrayList<Transaction> results);
    }


    @Override
    protected Void doInBackground(String... strings) {
        String api_id = null;

        try {
            api_id = URLEncoder.encode(strings[1], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        if (strings[0].equals(String.valueOf(0))) getAll(api_id);
        else getTransactions(api_id, strings[2], strings[3], strings[4], strings[5]);
        return null;
    }

    private void getTransactions(String api_id, String order, String month, String year, String type) {
        char c = month.charAt(0);
        if(month.length()==1)month="0"+c;
        String url1;
        if(type.equals("0")) url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/filter?month="+month+"&year="+year+"&sort="+order+"&page=";
        else url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/filter?month="+month+"&year="+year+"&typeId="+type+"&sort="+order+"&page=";

        URL url = null;
        try {
            int i = 0;
            while (true) {
                url1+=i;
                url = new URL(url1);

                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                int byteCharacter;
                String rezultat = "";
                while ((byteCharacter=in.read())!=-1){
                    rezultat+=(char)byteCharacter;
                }
                JSONObject jo = new JSONObject(rezultat);
                JSONArray results = jo.getJSONArray("transactions");
                if (results.length() == 0) break;
                for (int j = 0; j < results.length(); j++) {
                    JSONObject tr = results.getJSONObject(j);
                    int id = tr.getInt("id");
                    String title = tr.getString("title");
                    double amount = tr.getDouble("amount");
                    String date = tr.getString("date");
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = dateFormat.parse(date);
                    LocalDate ld = d.toInstant().atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    String itemDescription = tr.getString("itemDescription");
                    Integer ti = tr.optInt("transactionInterval");
                    String endDate = tr.getString("endDate");
                    LocalDate end = null;
                    if (!endDate.equals("null"))
                        end = dateFormat.parse(endDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    Type tip = getType(tr.getInt("TransactionTypeId"));
                    Transaction t = new Transaction(id, ld, amount, title, tip, itemDescription, ti, end);
                    transactions.add(t);
                    if((t.getType().getId()==1 || t.getType().getId()==2) && t.getEndDate()==null) {
                        transactions.remove(t);
                    }
                }
                i++;
                in.close();
                urlConnection.disconnect();
            }
                if (type.equals("0") || type.equals("1") || type.equals("2"))
                    addRegular(api_id, month, year);

    } catch (JSONException | IOException | ParseException e) {
        e.printStackTrace();
    }
    }

    private void addRegular(String api_id,  String month, String year) {

        try {
            int i = 0;
            while (true) {
                String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions?page=" + i;

                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                int byteCharacter;
                String rezultat = "";
                while ((byteCharacter=in.read())!=-1){
                    rezultat+=(char)byteCharacter;
                }
                JSONObject jo = new JSONObject(rezultat);
                JSONArray results = jo.getJSONArray("transactions");
                if (results.length() == 0) break;
                for (int j = 0; j < results.length(); j++) {
                    JSONObject tr = results.getJSONObject(j);
                    int id = tr.getInt("id");
                    String title = tr.getString("title");
                    double amount = tr.getDouble("amount");
                    String date = tr.getString("date");
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = dateFormat.parse(date);
                    LocalDate ld = d.toInstant().atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    String itemDescription = tr.getString("itemDescription");
                    Integer ti = tr.optInt("transactionInterval");
                    String endDate = tr.getString("endDate");
                    LocalDate end = null;
                    if (!endDate.equals("null"))
                        end = dateFormat.parse(endDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    Type type = getType(tr.getInt("TransactionTypeId"));
                    Transaction t = new Transaction(id, ld, amount, title, type, itemDescription, ti, end);
                    if((type.getId()==1 || type.getId()==2) && t.getEndDate()!=null){
                        getRegular(t, Integer.parseInt(month)-1, Integer.parseInt(year));
                    }
                }
                i++;
                in.close();
                urlConnection.disconnect();
            }

        } catch (IOException | JSONException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void getRegular(Transaction t, int month, int year) {
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.YEAR, t.getDate().getYear());
        ca.set(Calendar.MONTH, t.getDate().getMonthValue() - 1);
        ca.set(Calendar.DAY_OF_MONTH, t.getDate().getDayOfMonth());
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, t.getEndDate().getYear());
        c.set(Calendar.MONTH, t.getEndDate().getMonthValue() - 1);
        c.set(Calendar.DAY_OF_MONTH, t.getEndDate().getDayOfMonth());
        ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
        while (!ca.after(c) ) {
            if(year==0 || (ca.get(Calendar.MONTH)==month && ca.get(Calendar.YEAR)==year)){
                transactions.add(t);
            }

            ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
        }
    }

    private void getAll(String api_id) {
        try {
            int i = 0;
            while (true) {
                String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions?page=" + i;

                URL url = new URL(url1);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                int byteCharacter;
                String rezultat = "";
                while ((byteCharacter=in.read())!=-1){
                    rezultat+=(char)byteCharacter;
                }
                JSONObject jo = new JSONObject(rezultat);
                JSONArray results = jo.getJSONArray("transactions");
                if (results.length() == 0) break;
                for (int j = 0; j < results.length(); j++) {
                    JSONObject tr = results.getJSONObject(j);
                    int id = tr.getInt("id");
                    String title = tr.getString("title");
                    double amount = tr.getDouble("amount");
                    String date = tr.getString("date");
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date d = dateFormat.parse(date);
                    LocalDate ld = d.toInstant().atZone(ZoneId.systemDefault())
                            .toLocalDate();
                    String itemDescription = tr.getString("itemDescription");
                    Integer ti = tr.optInt("transactionInterval");
                    String endDate = tr.getString("endDate");
                    LocalDate end = null;
                    if (!endDate.equals("null"))
                        end = dateFormat.parse(endDate).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    Type type = getType(tr.getInt("TransactionTypeId"));
                    Transaction t = new Transaction(id, ld, amount, title, type, itemDescription, ti, end);
                    transactions.add(t);
                    if((t.getType().getId()==1 || t.getType().getId()==2) && t.getEndDate()==null) {
                        transactions.remove(t);
                    }

                }
                i++;
                in.close();
                urlConnection.disconnect();
            }

        } catch (IOException | JSONException | ParseException e) {
            e.printStackTrace();
        }
    }

    private Type getType(int id) {

        for (Type t : types) {
            if (t.getId() == id) return t;
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        caller.onDone(transactions);
    }

}
