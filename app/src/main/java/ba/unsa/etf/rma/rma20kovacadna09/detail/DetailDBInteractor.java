package ba.unsa.etf.rma.rma20kovacadna09.detail;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;
import ba.unsa.etf.rma.rma20kovacadna09.util.TransactionDBOpenHelper;

public class DetailDBInteractor {
    private Context context;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
    private ArrayList<Type> types;


    public DetailDBInteractor(Context context, ArrayList<Type> types) {
        this.context = context;
        this.types = types;
    }

    public Transaction getTransaction(int id) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_DESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_END_DATE,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_DELETED
        };

        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"), id);
        String order = null;
        Cursor cursor = cr.query(uri, kolone, null, null, order);
        Transaction t = null;
        if (cursor != null) {
            cursor.moveToFirst();

            t = new Transaction();
            int intIdPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
            int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
            int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
            int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
            int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
            int descPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION);
            int intPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
            int endDPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_END_DATE);
            int typePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
            int delPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DELETED);
            t.setInternalId(cursor.getInt(intIdPos));
            t.setId(cursor.getInt(idPos));
            Type tip = getType(cursor.getInt(typePos));
            t.setType(tip);
            t.setTitle(cursor.getString(titlePos));
            t.setAmount(cursor.getDouble(amountPos));
            t.setDeleted(cursor.getInt(delPos));
            try {
                t.setDate(df.parse(cursor.getString(datePos)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                t.setItemDescription(cursor.getString(descPos));
                t.setTransactionInterval(cursor.getInt(intPos));
                if (cursor.getString(endDPos) != null)
                    t.setEndDate(df.parse(cursor.getString(endDPos)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                else t.setEndDate(null);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            cursor.close();
        }
        return t;
    }

    public int addTransaction(Transaction transaction) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.transactions/elements");
        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.TRANSACTION_ID, transaction.getId());
        values.put(TransactionDBOpenHelper.TRANSACTION_TITLE, transaction.getTitle());
        values.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, transaction.getAmount());
        values.put(TransactionDBOpenHelper.TRANSACTION_DATE, transaction.getDate().toString());
        values.put(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION, transaction.getItemDescription());
        values.put(TransactionDBOpenHelper.TRANSACTION_INTERVAL, transaction.getTransactionInterval());
        String end = null;
        if (transaction.getEndDate() != null) end = transaction.getEndDate().toString();
        values.put(TransactionDBOpenHelper.TRANSACTION_END_DATE, end);
        values.put(TransactionDBOpenHelper.TRANSACTION_TYPE, transaction.getType().getId());
        if(transaction.getDeleted() == null)
            values.put(TransactionDBOpenHelper.TRANSACTION_DELETED, 0);
        else{
            values.put(TransactionDBOpenHelper.TRANSACTION_DELETED, transaction.getDeleted());
        }
        Uri ur = cr.insert(uri, values);
        int intId = -1;
        if (ur != null)
            intId = Integer.parseInt(ur.getLastPathSegment());
        return intId;
    }

    public int getId() {
        int id = 0;
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = Uri.parse("content://rma.provider.transactions/elements");
        String[] kolone = new String[]{
                "ifnull(MAX(" + TransactionDBOpenHelper.TRANSACTION_ID + "), 0)+1 "
        };
        Cursor cursor = cr.query(uri, kolone, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
            id = cursor.getInt(0);
            cursor.close();
        }
        return  id;
    };

    public void updateTransaction(Transaction transaction) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"), transaction.getId());

        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.TRANSACTION_TITLE, transaction.getTitle());
        values.put(TransactionDBOpenHelper.TRANSACTION_AMOUNT, transaction.getAmount());
        values.put(TransactionDBOpenHelper.TRANSACTION_DATE, transaction.getDate().toString());
        values.put(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION, transaction.getItemDescription());
        values.put(TransactionDBOpenHelper.TRANSACTION_INTERVAL, transaction.getTransactionInterval());
        String end = null;
        if (transaction.getEndDate() != null) end = transaction.getEndDate().toString();
        values.put(TransactionDBOpenHelper.TRANSACTION_END_DATE, end);
        values.put(TransactionDBOpenHelper.TRANSACTION_TYPE, transaction.getType().getId());
        cr.update(uri, values, null, null);
    }

    public void deleteTransaction(int id, int delete) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        Uri uri = ContentUris.withAppendedId(Uri.parse("content://rma.provider.transactions/elements"), id);
        ContentValues values = new ContentValues();
        values.put(TransactionDBOpenHelper.TRANSACTION_DELETED, delete);
        cr.update(uri, values, null, null);
    }

    private Type getType(int id) {

        for (Type t : types) {
            if (t.getId() == id) return t;
        }
        return null;
    }
}
