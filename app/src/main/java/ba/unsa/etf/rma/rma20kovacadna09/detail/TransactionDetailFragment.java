package ba.unsa.etf.rma.rma20kovacadna09.detail;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.account.AccountPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.account.IAccountPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.account.IAccountView;
import ba.unsa.etf.rma.rma20kovacadna09.data.Account;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;
import ba.unsa.etf.rma.rma20kovacadna09.list.ITypeView;
import ba.unsa.etf.rma.rma20kovacadna09.list.ITypesPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.list.SpinnerAdapter;
import ba.unsa.etf.rma.rma20kovacadna09.list.TypePresenter;

public class TransactionDetailFragment extends Fragment implements IAccountView, ITypeView, ITransactionDetailView {

    private boolean create = true;
    private EditText editTitle;
    private EditText editDate;
    private EditText editDescription;
    private EditText editInterval;
    private EditText editAmount;
    private EditText editEndDate;
    private Spinner editType;
    private Button save;
    private Button delete;
    private TextView offline;
    private SpinnerAdapter spinnerAdapter;
    private Transaction transaction;
    private Account account;
    private ITypesPresenter typesPresenter;
    private Transaction oldTransaction;
    private Transaction currentTransaction;
    private ArrayList<Type> types = new ArrayList<>();

    public ITypesPresenter getTypesPresenter() {
        if (typesPresenter == null) {
            typesPresenter = new TypePresenter(this, getActivity());
        }
        return typesPresenter;
    }

    @Override
    public void refreshView() {
        account = getAccountPresenter().getAccount();
    }

    @Override
    public void setTransaction(Transaction transaction) {
        this.transaction = transaction;
        currentTransaction = transaction;
        if (transaction != null) {
            delete.setEnabled(true);
            delete.setOnClickListener(deleteListener);
        }
    }

    @Override
    public void set() {

    }

    public interface OnSaveClick {
        public void onSaveClicked(Transaction newTransaction);
    }

    private OnSaveClick onSaveClick;

    private ITransactionDetailPresenter presenter;
    private IAccountPresenter accountPresenter;

    public ITransactionDetailPresenter getPresenter() {
        if (presenter == null) {
            presenter = new TransactionDetailPresenter(this, getActivity());
            getAccountPresenter().searchAccounts(getString(R.string.api_id));
        }
        return presenter;
    }

    public IAccountPresenter getAccountPresenter() {
        if (accountPresenter == null) {
            accountPresenter = new AccountPresenter(this, getActivity());
        }
        return accountPresenter;
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View detailView = inflater.inflate(R.layout.detail_fragment, container, false);
        if (getArguments() != null) {
            if (getArguments().containsKey("transaction"))
                getPresenter().setTransaction(getArguments().getParcelable("transaction"));

            types = new ArrayList<>();
            types.addAll(getTypesPresenter().getTypes());
            types.add(0, new Type("Choose type", 0));

            editAmount = detailView.findViewById(R.id.editAmount);
            editTitle = detailView.findViewById(R.id.editTitle);
            editDate = detailView.findViewById(R.id.editDate);
            editDescription = detailView.findViewById(R.id.editDescription);
            editEndDate = detailView.findViewById(R.id.editEndDate);
            editInterval = detailView.findViewById(R.id.editInterval);
            editType = detailView.findViewById(R.id.editTypeSpinner);
            save = detailView.findViewById(R.id.save);
            delete = detailView.findViewById(R.id.delete);
            offline = detailView.findViewById(R.id.offlineView);
            onSaveClick = (OnSaveClick) getActivity();
            setColors();

            transaction = getPresenter().getTransaction();

            spinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_item, new ArrayList());
            editType.setAdapter(spinnerAdapter);
            spinnerAdapter.setTipovi(types);
            int position = 0;
            if (transaction != null) {
                editAmount.setText(String.valueOf(transaction.getAmount()));
                editTitle.setText(transaction.getTitle());
                editDate.setText(transaction.getDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                if (transaction.getTransactionInterval() == null) editInterval.setText("");
                else
                    editInterval.setText(String.valueOf(transaction.getTransactionInterval()));
                editDescription.setText(transaction.getItemDescription());
                if (transaction.getEndDate() == null) editEndDate.setText("");
                else
                    editEndDate.setText(transaction.getEndDate().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
                position = transaction.getType().getId();
                editType.setSelection(position);
                delete.setOnClickListener(deleteListener);

                if (transaction.getDeleted() != null && (transaction.getDeleted() == 1 || transaction.getDeleted() == 3)) {
                    delete.setText(R.string.undo);
                    offline.setText("Offline brisanje");
                }

            } else {
                editType.setSelection(position);
                delete.setEnabled(false);
            }


            editAmount.addTextChangedListener(amountListener);
            editDate.addTextChangedListener(dateListener);
            editInterval.addTextChangedListener(intervalListener);
            editDescription.addTextChangedListener(descriptionListener);
            editEndDate.addTextChangedListener(endDateListener);
            editTitle.addTextChangedListener(onEditText);
            editType.setOnItemSelectedListener(onItemSelect);
            save.setOnClickListener(saveListener);

        }
        return detailView;
    }

    public boolean isConnected() {
        return ((ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    private BroadcastReceiver br = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm.getActiveNetworkInfo() == null) {
                getAccountPresenter().changeAccount(28, String.valueOf(account.getBudget()), String.valueOf(account.getMonthLimit()), String.valueOf(account.getTotalLimit()));
                delete.setOnClickListener(null);
                delete.setOnClickListener(onClickChange);

                if (transaction != null) {
                    if (transaction.getDeleted() != null && (transaction.getDeleted() == 1 || transaction.getDeleted() == 3)) {
                        delete.setText(R.string.undo);
                        offline.setText("Offline brisanje");
                    } else offline.setText("Offline izmjena");
                } else {
                    offline.setText("Offline dodavanje");
                }
            } else {
                offline.setText("");
                delete.setOnClickListener(null);
                delete.setOnClickListener(deleteListener);
                delete.setText(R.string.delete);
            }
        }
    };

    private IntentFilter filter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");


    @Override
    public void onResume() {
        super.onResume();
        getActivity().registerReceiver(br, filter);
    }

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(br);
        super.onPause();
    }


    private View.OnClickListener onClickChange = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (delete.getText().equals("Delete")) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                alertDialog.setTitle("Confirmation");
                alertDialog.setTitle("Da li želite obrisati? ");
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        delete.setText(R.string.delete);
                    }
                });
                alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        delete.setText(R.string.undo);
                        offline.setText("Offline brisanje");
                        if (transaction.getDeleted() == 2) {
                            getPresenter().removeTransaction(transaction.getId(), 3);
                        } else getPresenter().removeTransaction(transaction.getId(), 1);
                        dialog.dismiss();
                    }
                });
                alertDialog.create().show();
            }

            else if (delete.getText().equals("Undo")) {
                if (transaction.getDeleted() == 3) {
                    getPresenter().removeTransaction(transaction.getId(), 2);
                } else
                    getPresenter().removeTransaction(transaction.getId(), 0);
                delete.setText(R.string.delete);
                offline.setText("Offline izmjena");
            }
        }

    };


    private View.OnClickListener deleteListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            alertDialog.setTitle("Confirmation");
            alertDialog.setTitle("Da li ste sigurni da želite obrisati transakciju?");
            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getPresenter().removeTransaction(transaction.getId(), 1);
                    getPresenter().setTransaction(null);
                    transaction = getPresenter().getTransaction();
                    getAccountPresenter().changeBudget(account.getId());
                    onSaveClick.onSaveClicked(null);
                    dialog.dismiss();
                }
            });
            alertDialog.create().show();
        }
    };

    private View.OnClickListener saveListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            try {
                boolean toDo = true;
                if (getArguments().getParcelable("transaction") == null) {
                    toDo = Validate();
                }
                if (editType.getSelectedItemPosition() == 0) {
                    editType.setBackgroundColor(Color.parseColor("#FCA5AB"));
                    toDo = false;
                }
                if (toDo) {
                    SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
                    LocalDate ed = null;
                    LocalDate pd = LocalDate.now();
                    String fool = null;
                    if (!editDate.getText().toString().isEmpty()) {
                        pd = df.parse(editDate.getText().toString()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    }

                    if (!editInterval.getText().toString().isEmpty())
                        fool = editInterval.getText().toString();
                    if (!editEndDate.getText().toString().isEmpty()) {
                        ed = df.parse(editEndDate.getText().toString()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                    }

                    String desc = null;
                    if (!editDescription.getText().toString().isEmpty())
                        desc = editDescription.getText().toString();
                    oldTransaction = transaction;
                    Integer id = null;
                    Integer intId = null;
                    if (oldTransaction != null) {
                        id = oldTransaction.getId();
                        intId = oldTransaction.getInternalId();
                    }
                    if (!isConnected() && intId == null) oldTransaction = null;
                    getPresenter().create(intId, id, pd, Double.parseDouble(editAmount.getText().toString()), editTitle.getText().toString(), (Type) editType.getSelectedItem(), desc, fool, ed);

                    currentTransaction = getPresenter().getTransaction();
                    if (currentTransaction.getType().getName().equals("Regular payment") || currentTransaction.getType().getName().equals("Individual payment") || currentTransaction.getType().getName().equals("Purchase")) {
                        if (getPresenter().getCurrentLimit(currentTransaction, false) >= account.getMonthLimit() || getPresenter().getCurrentLimit(currentTransaction, true) >= account.getTotalLimit()) {
                            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
                            alertDialog.setTitle("Confirmation");
                            alertDialog.setMessage("Transakcija premašuje limit. Da li ste sigurni da želite potvrditi?");
                            alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    save(oldTransaction, currentTransaction);
                                    dialog.dismiss();
                                }
                            });

                            alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getPresenter().setTransaction(oldTransaction);
                                    dialog.dismiss();
                                }
                            });
                            alertDialog.create().show();
                        } else {
                            save(oldTransaction, currentTransaction);
                        }
                    } else {
                        save(oldTransaction, currentTransaction);
                    }
                }
            } catch (ParseException e) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
            }
        }
    };

    private void save(Transaction oldTransaction, Transaction currentTransaction) {
        if (oldTransaction == null) {
            getPresenter().addTransaction(currentTransaction);
        } else {
            getPresenter().updateTransaction(currentTransaction);

        }
        getAccountPresenter().changeBudget(28);
        transaction = getPresenter().getTransaction();
        onSaveClick.onSaveClicked(currentTransaction);
        setColors();
        Toast.makeText(getActivity(), "Saved", Toast.LENGTH_SHORT).show();
    }

    private boolean Validate() {
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        boolean toDo = true;
        if (editDate.getText().toString().isEmpty()) {
            editDate.setBackgroundColor(Color.parseColor("#FCA5AB"));
            toDo = false;
        }
        if (editTitle.getText().toString().isEmpty() || editTitle.getText().toString().length() <= 3 || editTitle.getText().toString().length() >= 15) {
            editTitle.setBackgroundColor(Color.parseColor("#FCA5AB"));
            toDo = false;
        }
        if (editType.getSelectedItemPosition() == 0) {
            editType.setBackgroundColor(Color.parseColor("#FCA5AB"));
            toDo = false;
        }
        if (editAmount.getText().toString().isEmpty()) {
            editAmount.setBackgroundColor(Color.parseColor("#FCA5AB"));
            toDo = false;
        }
        if ((editType.getSelectedItemPosition() == 5 || editType.getSelectedItemPosition() == 1 || editType.getSelectedItemPosition() == 3)
                && editDescription.getText().toString().isEmpty()) {
            editDescription.setBackgroundColor(Color.parseColor("#FCA5AB"));
            toDo = false;
        }

        if (editType.getSelectedItemPosition() == 1 || editType.getSelectedItemPosition() == 2) {
            if (editInterval.getText().toString().length() == 0) {
                toDo = false;
                editInterval.setBackgroundColor(Color.parseColor("#FCA5AB"));
            }

            try {
                if (editEndDate.getText().toString().length() == 0 || df.parse(editDate.getText().toString()).equals(df.parse(editEndDate.getText().toString())) || df.parse(editEndDate.getText().toString()).before(df.parse(editDate.getText().toString()))) {
                    editEndDate.setBackgroundColor(Color.parseColor("#FCA5AB"));
                    toDo = false;
                }
            } catch (ParseException e) {
                Toast.makeText(getActivity(), "Error", Toast.LENGTH_SHORT).show();
                editEndDate.setBackgroundColor(Color.parseColor("#FCA5AB"));
            }
        }
        return toDo;
    }

    private AdapterView.OnItemSelectedListener onItemSelect = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (create) {
                create = false;
            } else {
                if (!parent.getSelectedItem().toString().equals("Choose type"))
                    editType.setBackgroundColor(Color.parseColor("#DAF7A6"));
                else
                    editType.setBackgroundColor(Color.parseColor("#FCA5AB"));
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    private TextWatcher amountListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0)
                editAmount.setBackgroundColor(Color.parseColor("#DAF7A6"));
            else
                editAmount.setBackgroundColor(Color.parseColor("#FCA5AB"));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    private TextWatcher onEditText = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0)
                editTitle.setBackgroundColor(Color.parseColor("#DAF7A6"));
            else
                editTitle.setBackgroundColor(Color.parseColor("#FCA5AB"));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private TextWatcher dateListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0)
                editDate.setBackgroundColor(Color.parseColor("#DAF7A6"));
            else
                editDate.setBackgroundColor(Color.parseColor("#FCA5AB"));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private TextWatcher intervalListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0)
                editInterval.setBackgroundColor(Color.parseColor("#DAF7A6"));
            else
                editInterval.setBackgroundColor(Color.parseColor("#FCA5AB"));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private TextWatcher descriptionListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0)
                editDescription.setBackgroundColor(Color.parseColor("#DAF7A6"));
            else
                editDescription.setBackgroundColor(Color.parseColor("#FCA5AB"));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private TextWatcher endDateListener = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.length() != 0)
                editEndDate.setBackgroundColor(Color.parseColor("#DAF7A6"));
            else
                editEndDate.setBackgroundColor(Color.parseColor("#FCA5AB"));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void setColors() {
        int c = Color.parseColor("#EFEFEF");
        editInterval.setBackgroundColor(c);
        editTitle.setBackgroundColor(c);
        editEndDate.setBackgroundColor(c);
        editDescription.setBackgroundColor(c);
        editAmount.setBackgroundColor(c);
        editType.setBackgroundColor(c);
        editDate.setBackgroundColor(c);
        create = true;
    }
}

