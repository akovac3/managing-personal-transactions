package ba.unsa.etf.rma.rma20kovacadna09.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Locale;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Account;

public class BudgetFragment extends Fragment implements IAccountView{
    private TextView amount;
    private EditText monthLimit;
    private EditText totalLimit;
    private Button save;
    private Account account;

    private IAccountPresenter accountPresenter;

    private IAccountPresenter getPresenter() {
        if (accountPresenter == null) {
            accountPresenter = new AccountPresenter(this, getActivity());
        }
        return accountPresenter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.budget_fragment, container, false);
        amount = view.findViewById(R.id.budgetAmount);
        monthLimit = view.findViewById(R.id.editMonthLimit);
        totalLimit = view.findViewById(R.id.editTotalLimit);
        save = view.findViewById(R.id.saveButton);

        refresh();
        save.setOnClickListener(saveClickListener);
        return view;
    }


    private View.OnClickListener saveClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            getPresenter().changeAccount(28,null, monthLimit.getText().toString(), totalLimit.getText().toString());
            Toast.makeText(getActivity(), "Saved", Toast.LENGTH_SHORT).show();
        }
    };

    private void refresh() {
        getPresenter().searchAccounts(getString(R.string.api_id));
    }


    @Override
    public void onResume() {
        super.onResume();
        refresh();
    }


    @Override
    public void refreshView() {
        account = getPresenter().getAccount();
        if(account!=null) {
            String out = String.format(Locale.ENGLISH, "%.2f", account.getBudget());
            String total = String.format(Locale.ENGLISH, "%.2f", account.getTotalLimit());
            String month = String.format(Locale.ENGLISH, "%.2f", account.getMonthLimit());
            totalLimit.setText(total);
            amount.setText(out);
            monthLimit.setText(month);
        }
    }
}
