package ba.unsa.etf.rma.rma20kovacadna09.list;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;

public interface ITransactionView {
    void setTransactions(ArrayList<Transaction> transactions);
    void notifyTransactionListDataSetChanged();
}
