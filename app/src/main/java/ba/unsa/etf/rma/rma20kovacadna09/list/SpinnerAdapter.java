package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class SpinnerAdapter extends ArrayAdapter<Type> {
    private int resource;
    private ImageView icon;
    private TextView name;

    public SpinnerAdapter(@NonNull Context context, int _resource, ArrayList items) {
        super(context, _resource, items);
        resource = _resource;
    }

    public void setTipovi(ArrayList<Type> types){
        this.addAll(types);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView, parent);
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        return initView(position,convertView, parent);
    }

    public View initView(int position, View convertView, ViewGroup parent){
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout) convertView;
        }

        Type type = getItem(position);
        icon = newView.findViewById(R.id.iconView);
        name = newView.findViewById(R.id.name);

        name.setText(type.getName());
        try {
            Class res = R.drawable.class;
            String tip = type.getName().replaceAll(" ", "");
            Field field = res.getField(tip.toLowerCase());
            int drawableId = field.getInt(null);
            icon.setImageResource(drawableId);
        }
        catch (Exception e) {
            icon.setImageResource(R.drawable.transaction);
        }
        return newView;
    }


}
