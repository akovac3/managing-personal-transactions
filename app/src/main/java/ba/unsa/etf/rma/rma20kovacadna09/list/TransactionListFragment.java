package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.account.AccountPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.account.IAccountPresenter;
import ba.unsa.etf.rma.rma20kovacadna09.account.IAccountView;
import ba.unsa.etf.rma.rma20kovacadna09.data.Account;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class TransactionListFragment extends Fragment implements ITransactionView, IAccountView, ITypeView {
    private TextView amountView;
    private TextView limitView;
    private TextView monthView;
    private Spinner filterSpinner;
    private Spinner sortSpinner;
    private ListView listView;
    private Button leftButton;
    private Button rightButton;
    private Button addButton;
    private Calendar c = Calendar.getInstance();
    private Date date = c.getTime();
    private DateFormat dateFormat;
    private Transaction currentTransaction;
    private ArrayList<Type> types= new ArrayList<>();

    private ITransactionPresenter presenter;
    private IAccountPresenter accountPresenter;
    private ITypesPresenter typesPresenter;

    private TransactionListAdapter adapter;
    private SpinnerAdapter spinnerAdapter;
    private ArrayAdapter<CharSequence> sortAdapter;
    private Account account;

    public IAccountPresenter getAccountPresenter() {
        if (accountPresenter == null) {
            accountPresenter = new AccountPresenter(this, getActivity());
        }
        return accountPresenter;
    }

    public ITransactionPresenter getPresenter() {
        if (presenter == null) {
            presenter = new TransactionPresenter(this, getActivity());
        }
        return presenter;
    }

    public ITypesPresenter getTypesPresenter(){
        if (typesPresenter == null) {
            typesPresenter = new TypePresenter(this, getActivity());
        }
        return typesPresenter;
    }

    private OnItemClick onItemClick;

    @Override
    public void refreshView() {
        account = getAccountPresenter().getAccount();
        if(account!=null) {
            String out = "Global amount: " + String.format(Locale.ENGLISH, "%.2f", account.getBudget());
            String limit = "Limit: " + String.format(Locale.ENGLISH, "%.2f", account.getTotalLimit());
            limitView.setText(limit);
            amountView.setText(out);
        }
    }


    @Override
    public void set() {

    }

    public interface OnItemClick {
        public void onItemClicked(Transaction transaction);
    }

    private IntentFilter filter = new IntentFilter("ba.unsa.rma.ACTION");

    @Override
    public void onPause() {
        getActivity().unregisterReceiver(br);
        super.onPause();
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        View fragmentView = inflater.inflate(R.layout.list_fragment, container, false);
        dateFormat = new SimpleDateFormat("MMMM, yyyy");

        amountView = fragmentView.findViewById(R.id.amountView);
        limitView = fragmentView.findViewById(R.id.limitView);
        monthView = fragmentView.findViewById(R.id.monthView);
        filterSpinner = fragmentView.findViewById(R.id.filterSpinner);
        sortSpinner = fragmentView.findViewById(R.id.sortSpinner);
        leftButton = fragmentView.findViewById(R.id.leftButton);
        rightButton = fragmentView.findViewById(R.id.rightButton);
        addButton = fragmentView.findViewById(R.id.addButton);
        listView = fragmentView.findViewById(R.id.listView);


        types= new ArrayList<>();
        types.addAll(getTypesPresenter().getTypes());
        types.add(0, new Type("All", 0));

        getPresenter().setTypes(types);
        monthView.setText(dateFormat.format(date));
        onItemClick = (OnItemClick) getActivity();

        adapter = new TransactionListAdapter(getActivity(), R.layout.transactions_list, new ArrayList<Transaction>());
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(listOnClickListener);
        sortAdapter = ArrayAdapter.createFromResource(getActivity(), R.array.sortItems, android.R.layout.simple_spinner_item);
        sortAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sortSpinner.setAdapter(sortAdapter);
        sortSpinner.setOnItemSelectedListener(sortSelectListener);

        leftButton.setOnClickListener(leftClickListener);
        rightButton.setOnClickListener(rightClickListener);

        spinnerAdapter = new SpinnerAdapter(getActivity(), R.layout.spinner_item, new ArrayList());
        filterSpinner.setAdapter(spinnerAdapter);
        filterSpinner.setOnItemSelectedListener(filterSelectListener);
        spinnerAdapter.setTipovi(types);

        addButton.setOnClickListener(addListener);
        filterSpinner.setBackgroundColor(Color.parseColor("#EFEFEF"));
        sortSpinner.setBackgroundColor(Color.parseColor("#EFEFEF"));

        return fragmentView;
    }

    private View.OnClickListener addListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            onItemClick.onItemClicked(null);
        }
    };

    private View.OnClickListener leftClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            c.add(Calendar.MONTH, -1);
            date = c.getTime();
            monthView.setText(dateFormat.format(date));
            getPresenter().getTransactions(sortSpinner.getSelectedItemPosition(), c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR), filterSpinner.getSelectedItemPosition());

        }

    };

    private View.OnClickListener rightClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            c.add(Calendar.MONTH, 1);
            date = c.getTime();
            monthView.setText(dateFormat.format(date));
            getPresenter().getTransactions(sortSpinner.getSelectedItemPosition(), c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR), filterSpinner.getSelectedItemPosition());
        }
    };

    private AdapterView.OnItemSelectedListener sortSelectListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        getPresenter().getTransactions(position, c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR), filterSpinner.getSelectedItemPosition());

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    private AdapterView.OnItemSelectedListener filterSelectListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            getPresenter().getTransactions(sortSpinner.getSelectedItemPosition(), c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR), position);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    };

    public void refresh(Transaction newTransaction){
        currentTransaction = newTransaction;
        if(newTransaction!=null){
            c.set(Calendar.MONTH, newTransaction.getDate().getMonthValue()-1);
            c.set(Calendar.YEAR, newTransaction.getDate().getYear());
            date = c.getTime();
            monthView.setText(dateFormat.format(date));
        }

        getPresenter().getTransactions(sortSpinner.getSelectedItemPosition(), c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR), filterSpinner.getSelectedItemPosition());
        if(newTransaction!=null){
            listView.setSelection(adapter.getPosition(newTransaction));
            listView.setSelector(R.color.colorPaleYellow);
        }

        getAccountPresenter().changeBudget(account.getId());
    }

    private BroadcastReceiver br = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if("ba.unsa.rma.ACTION".equals(intent.getAction())){
                getPresenter().getTransactions(sortSpinner.getSelectedItemPosition(), c.get(Calendar.MONTH)+1, c.get(Calendar.YEAR), filterSpinner.getSelectedItemPosition());
                getAccountPresenter().changeBudget(28);
            }

        }
    };

            @Override
    public void setTransactions(ArrayList<Transaction> transactions) {
        adapter.clear();
        adapter.setTransactions(transactions);
    }

    @Override
    public void notifyTransactionListDataSetChanged() {
        adapter.notifyDataSetChanged();
    }

    private AdapterView.OnItemClickListener listOnClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

            Transaction transaction = adapter.getTransaction(position);
            if(currentTransaction==transaction && getActivity().findViewById(R.id.transactions_detail)!=null){
                currentTransaction=null;
                listView.setSelector(R.color.background_material_light);

                onItemClick.onItemClicked(null);
            }
            else {
                currentTransaction = transaction;
                listView.setSelector(R.color.colorPaleYellow);
                onItemClick.onItemClicked(transaction);
                notifyTransactionListDataSetChanged();
            }
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        getAccountPresenter().searchAccounts(getString(R.string.api_id));
        getActivity().registerReceiver(br, filter);
    }
}
