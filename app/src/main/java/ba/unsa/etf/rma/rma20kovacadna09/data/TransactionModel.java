package ba.unsa.etf.rma.rma20kovacadna09.data;


import java.time.LocalDate;
import java.util.ArrayList;

public class TransactionModel {

    public static ArrayList<Transaction> transactions = new ArrayList<Transaction>() {
        {
            add(new Transaction(LocalDate.of(2020,1,5), 90.9, "telemach racun", new Type("Regular payment", 1), "racun za internet", 30, LocalDate.of(2020, 3, 7)));
            add(new Transaction(LocalDate.of(2020,3,25), 100, "youtube", new Type("Individual income", 4), null, null, null));
            add(new Transaction(LocalDate.of(2020,3,23), 130.5, "mercator", new Type("Purchase", 3), "kupovina namjernica u mercatoru ", null, null));
            add(new Transaction(LocalDate.of(2020,1,1), 3000, "plata", new Type("Regular income", 2), null, 31, LocalDate.of(2020,4,4)));
            add(new Transaction(LocalDate.of(2020,3,15), 240, "astra kupovina", new Type("Individual payment", 5), "kupovina patika Guess", null, null));
            add(new Transaction(LocalDate.of(2020,1,10), 80.5, "struja", new Type("Regular payment", 1), "racun za struju ", 29, LocalDate.of(2020,4,13)));
            add(new Transaction(LocalDate.of(2020,2,8), 95.5, "bhtelecom", new Type("Regular payment", 1), "racun za telefon ", 30, LocalDate.of(2020,4,10)));
            add(new Transaction(LocalDate.of(2020,2,25), 1000, "google", new Type("Individual income", 4), null, null, null));
            add(new Transaction(LocalDate.of(2020,2,23), 500, "uplata za app", new Type("Regular income", 2), null, 30, LocalDate.of(2020,4,25)));
            add(new Transaction(LocalDate.of(2020,3,8), 990.5, "xyz racun", new Type("Individual payment", 5), "kupovina jakne", null, null));
            add(new Transaction(LocalDate.of(2020,2,11), 250, "Inoma racun", new Type("Individual payment", 5), "kupovina parfema",null , null));
            add(new Transaction(LocalDate.of(2020,2,14), 75.5, "4seasons racun", new Type("Individual payment", 5), "racun za rucak ", null, null));
            add(new Transaction(LocalDate.of(2020,2,13), 25, "instagram", new Type("Individual income", 4), null,null , null));
            add(new Transaction(LocalDate.of(2020,1,16), 150, "atstore racun", new Type("Individual payment", 5), "rata za mobitel ", null, null));
            add(new Transaction(LocalDate.of(2020,1,18), 420.6, "zara kupovina", new Type("Individual payment", 5), "kupovina u zari ", null, null));
            add(new Transaction(LocalDate.of(2020,1,25), 154.8, "cm racun", new Type("Individual payment", 5), "kupovina kozmetike", null, null));
            add(new Transaction(LocalDate.of(2020,3,2), 220.5, "sportvision", new Type("Purchase", 3), "kupovina sportske opreme ", null, null));
            add(new Transaction(LocalDate.of(2020,1,14), 350, "amazon", new Type("Individual income", 4), null, null, null));
            add(new Transaction(LocalDate.of(2020,2,21), 182.6, "konzum racun", new Type("Purchase", 3), "nabavka namjernica u konzumu", null, null));
            add(new Transaction(LocalDate.of(2020,3,9), 150, "uplata ", new Type("Individual income", 4), null, null, null));

        }
    };
}
