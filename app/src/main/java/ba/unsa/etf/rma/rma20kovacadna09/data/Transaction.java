package ba.unsa.etf.rma.rma20kovacadna09.data;

import android.os.Parcel;
import android.os.Parcelable;

import java.time.LocalDate;

public class Transaction implements Parcelable {
    private LocalDate date;
    private double amount;
    private String title = null;
    private Type type;
    private String itemDescription = null;
    private Integer transactionInterval = null;
    private LocalDate endDate = null;
    private Integer id;
    private Integer internalId;
    private Integer deleted;

    public Transaction() {
    }

    public Transaction(Integer id, LocalDate date, double amount, String title, Type type, String itemDescription, Integer transactionInterval, LocalDate endDate) {
        this.id=id;
        this.date = date;
        this.amount = amount;
        setTitle(title);
        this.type = type;
        setItemDescription(itemDescription);
        setTransactionInterval(transactionInterval);
        setEndDate(endDate);
    }

    public Transaction(Integer internalId, Integer id, LocalDate date, double amount, String title, Type type, String itemDescription, Integer transactionInterval, LocalDate endDate) {
        this.internalId=internalId;
        this.id=id;
        this.date = date;
        this.amount = amount;
        setTitle(title);
        this.type = type;
        setItemDescription(itemDescription);
        setTransactionInterval(transactionInterval);
        setEndDate(endDate);
    }

    public Transaction(Integer internalId, Integer id, LocalDate date, double amount, String title, Type type, String itemDescription, Integer transactionInterval, LocalDate endDate, Integer deleted) {
        this.internalId=internalId;
        this.id=id;
        this.date = date;
        this.amount = amount;
        setTitle(title);
        this.type = type;
        setItemDescription(itemDescription);
        setTransactionInterval(transactionInterval);
        setEndDate(endDate);
        this.deleted = deleted;
    }

    public Transaction(LocalDate date, double amount, String title, Type type, String itemDescription, Integer transactionInterval, LocalDate endDate) {
        this.date = date;
        this.amount = amount;
        setTitle(title);
        this.type = type;
        setItemDescription(itemDescription);
        setTransactionInterval(transactionInterval);
        setEndDate(endDate);
    }

    protected Transaction(Parcel in){
        id = in.readInt();
        date = (LocalDate) in.readSerializable();
        amount = in.readDouble();
        title = in.readString();
        type = (Type) in.readSerializable();
        itemDescription = in.readString();
        transactionInterval = in.readInt();
        endDate = (LocalDate) in.readSerializable();
        deleted = in.readInt();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public Integer getInternalId() {
        return internalId;
    }

    public void setInternalId(Integer internalId) {
        this.internalId = internalId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        if(title.length()<=3 || title.length()>15)
            throw new RuntimeException("Neispravna velicina titlea-a");
        this.title = title;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getItemDescription() {

        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        if(!type.getName().equals("Individual income") && !type.getName().equals("Regular income"))
            this.itemDescription = itemDescription;
    }

    public Integer getTransactionInterval() {
        return transactionInterval;
    }

    public void setTransactionInterval(Integer transactionInterval) {
        if(type.getName().equals("Regular income") || type.getName().equals("Regular payment"))
            this.transactionInterval = transactionInterval;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        if(type.getName().equals("Regular income") || type.getName().equals("Regular payment"))
            this.endDate = endDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeSerializable(date);
        dest.writeDouble(amount);
        dest.writeString(title);
        dest.writeString(this.type.getName());
        dest.writeString(itemDescription);
        dest.writeInt(transactionInterval);
        dest.writeSerializable(endDate);
        dest.writeInt(deleted);
    }
}
