package ba.unsa.etf.rma.rma20kovacadna09.detail;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class TransactionDetailInteractor extends AsyncTask<String, Void, Void> {
    private OnDADone caller;
    private int result;

    public TransactionDetailInteractor(OnDADone caller) {
        this.caller = caller;
    }

    public interface OnDADone {
        public void onDone(int result);
    }

    @Override
    protected Void doInBackground(String... strings) {
        if (strings[0].equals("0")) deleteTransaction(strings[1], strings[2]);
        else if (strings[0].equals("1"))
            addTransaction(strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7], strings[8]);
        else if (strings[0].equals("2"))
            updateTransaction(strings[1], strings[2], strings[3], strings[4], strings[5], strings[6], strings[7], strings[8], strings[9]);
        return null;
    }

    private void updateTransaction(String api_id, String id, String date, String title, String amount, String endDate, String description, String interval, String tipId) {
        JSONObject jTransaction = new JSONObject();
        Integer data;
        if (interval != null) data = Integer.parseInt(interval);
        else data = null;

        try {
            jTransaction.put("date", date);

            jTransaction.put("title", title);
            jTransaction.put("amount", Double.parseDouble(amount));
            jTransaction.put("endDate", endDate);
            if(description == null || description.length()==0)jTransaction.put("itemDescription", JSONObject.NULL);
            else jTransaction.put("itemDescription", description);
            jTransaction.put("transactionInterval", data);
            jTransaction.put("TransactionTypeId", Integer.parseInt(tipId));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            api_id = URLEncoder.encode(api_id, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/"+id;

        URL url;
        try {

            url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);


            OutputStream dos = urlConnection.getOutputStream();
            dos.write(jTransaction.toString().getBytes());
            dos.flush();
            dos.close();
            urlConnection.connect();

            InputStream is = urlConnection.getInputStream();
            String result = "";
            int byteCharacter;
            while ((byteCharacter = is.read()) != -1) {
                result += (char) byteCharacter;
            }

            is.close();
            urlConnection.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addTransaction(String api_id, String date, String title, String amount, String endDate, String description, String interval, String tipId) {
        JSONObject jTransaction = new JSONObject();

        try {
            api_id = URLEncoder.encode(api_id, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions";

        URL url;
        try {
            Integer data;
            if (interval != null) data = Integer.parseInt(interval);
            else data = null;
            jTransaction.put("date", date);
            jTransaction.put("title", title);
            jTransaction.put("amount", Double.parseDouble(amount));
            if (endDate == null) jTransaction.put("endDate", JSONObject.NULL);
            else jTransaction.put("endDate", endDate);
            if (description == null || description.length()==0) jTransaction.put("itemDescription", JSONObject.NULL);
            else jTransaction.put("itemDescription", description);
            if (data == null) jTransaction.put("transactionInterval", JSONObject.NULL);
            else jTransaction.put("transactionInterval", data);
            jTransaction.put("typeId", Integer.parseInt(tipId));
            url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setRequestProperty("Content-Type", "application/json");
            urlConnection.setRequestProperty("Accept", "application/json");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);


            OutputStream dos = urlConnection.getOutputStream();
            dos.write(jTransaction.toString().getBytes());
            dos.flush();
            dos.close();
            urlConnection.connect();

            InputStream is = urlConnection.getInputStream();
            String result = "";
            int byteCharacter;
            while ((byteCharacter = is.read()) != -1) {
                result += (char) byteCharacter;
            }
            JSONObject jo = new JSONObject(result);
            this.result = jo.getInt("id");
            is.close();
            urlConnection.disconnect();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
    }

    private void deleteTransaction(String api_id, String id) {
        try {
            api_id = URLEncoder.encode(api_id, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/" + api_id + "/transactions/" + id;

        URL url;

        try {
            url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("DELETE");
            urlConnection.setDoOutput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();

            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String rezultat = convertStreamToString(in);

            JSONObject jo = new JSONObject(rezultat);
            String result = jo.getString("success");

            Log.d("json api", result);
            in.close();
            urlConnection.disconnect();

        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


    }


    private String convertStreamToString(InputStream in) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                in.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        caller.onDone(result);
    }


}
