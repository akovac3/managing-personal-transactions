package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.lang.reflect.Field;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;

public class TransactionListAdapter extends ArrayAdapter<Transaction> {
    private int resource;
    private ImageView transactionImage;
    private TextView title;
    private TextView amount;

    public TransactionListAdapter(@NonNull Context context, int _resource, ArrayList<Transaction> items) {
        super(context, _resource, items);
        resource = _resource;
    }

    public void setTransactions(ArrayList<Transaction> transactions){
        this.addAll(transactions);
    }

    public Transaction getTransaction(int position) {
        return this.getItem(position);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LinearLayout newView;
        if (convertView == null) {
            newView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater li;
            li = (LayoutInflater) getContext().
                    getSystemService(inflater);
            li.inflate(resource, newView, true);
        } else {
            newView = (LinearLayout) convertView;
        }

       Transaction transaction = getItem(position);

        transactionImage = newView.findViewById(R.id.transactionImage);
        title = newView.findViewById(R.id.title);
        amount = newView.findViewById(R.id.amount);
        title.setText(transaction.getTitle());
        amount.setText(String.valueOf(transaction.getAmount()));


        String iconMatch = transaction.getType().getName().replaceAll(" ", "");
        try {
            Class res = R.drawable.class;
            Field field = res.getField(iconMatch.toLowerCase());
            int drawableId = field.getInt(null);
            transactionImage.setImageResource(drawableId);
        }
        catch (Exception e) {
            transactionImage.setImageResource(R.drawable.transaction);
        }

        return newView;
    }
}
