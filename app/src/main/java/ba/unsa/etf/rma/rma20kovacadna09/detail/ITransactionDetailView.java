package ba.unsa.etf.rma.rma20kovacadna09.detail;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;

public interface ITransactionDetailView {
    void setTransaction(Transaction transaction);
}
