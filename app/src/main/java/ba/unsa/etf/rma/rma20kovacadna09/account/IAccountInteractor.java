package ba.unsa.etf.rma.rma20kovacadna09.account;

import ba.unsa.etf.rma.rma20kovacadna09.data.Account;

public interface IAccountInteractor {
    Account getAccount();
    void searchAccount(String api_id, int id);
    void changeAccount(String api_id, int id, String budget, String monthLimit, String totalLimit);
    void insertIntoDB(Account account);
    void deleteAccount();
    void deleteAllAccount();
}
