package ba.unsa.etf.rma.rma20kovacadna09.account;

import ba.unsa.etf.rma.rma20kovacadna09.data.Account;

public interface IAccountPresenter {
    void searchAccounts(String query);
    Account getAccount();
    void setAccount(Account account);
    void changeAccount(int id, String budget, String monthLimit, String totalLimit);
    void changeBudget(int id);
    void writeAccountInDB(Account account);
    void deleteAccount();
    Account getDBAccount();
    void deleteAll();
}
