package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;
import ba.unsa.etf.rma.rma20kovacadna09.util.TransactionDBOpenHelper;

public class TransactionDBInteractor {
    private ArrayList<Transaction> transactions;
    private ArrayList<Type> types;
    private Context context;
    private SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");


    public TransactionDBInteractor(Context context, ArrayList<Type> types) {
        this.types = types;
        this.context = context;
        transactions = new ArrayList<>();
    }

    public ArrayList<Transaction> getTransactionsMonthly(int month, int year) {
        String mn = String.valueOf(month);
        char c = mn.charAt(0);
        if(mn.length()==1)mn="0"+c;
        String where = " strftime('%m'," + TransactionDBOpenHelper.TRANSACTION_DATE + ")=? AND strftime('%Y'," + TransactionDBOpenHelper.TRANSACTION_DATE + ")=?";
        String[] whereArgs = {mn, String.valueOf(year)};
        getTransactions(where, whereArgs);
        addRegular(month, year);
        return transactions;
    }

    private void addRegular(int month, int year) {
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_DESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_END_DATE,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_DELETED
        };

        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String where = TransactionDBOpenHelper.TRANSACTION_TYPE + " = 1" +" OR "+ TransactionDBOpenHelper.TRANSACTION_TYPE +" = 2";
        String[] whereArgs = null;
        String order = null;
        Cursor cursor = cr.query(adresa, kolone, where, whereArgs, order);
        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            Transaction t = null;
            do {
                t= new Transaction();
                int intIdPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
                int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
                int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
                int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
                int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
                int descPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION);
                int intPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
                int endDPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_END_DATE);
                int typePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
                int deletedPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DELETED);
                t.setInternalId(cursor.getInt(intIdPos));
                t.setId(cursor.getInt(idPos));
                String title = cursor.getString(titlePos);
                t.setTitle(title);
                t.setType(getType(cursor.getInt(typePos)));
                t.setAmount(cursor.getDouble(amountPos));
                t.setDeleted(cursor.getInt(deletedPos));
                try {
                    t.setDate(df.parse(cursor.getString(datePos)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                    t.setItemDescription(cursor.getString(descPos));
                    t.setTransactionInterval(cursor.getInt(intPos));
                    if(cursor.getString(endDPos)!=null){
                        LocalDate ld = df.parse(cursor.getString(endDPos)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
                        t.setEndDate(ld);

                    }
                    else t.setEndDate(null);
                } catch (ParseException e) {
                    e.printStackTrace();
                }


                if(t.getEndDate()!=null) {
                    getRegular(t, month - 1, year);
                }
            } while(cursor.moveToNext());
            cursor.close();
        }

    }

    public ArrayList<Transaction> getAllTransactions() {
        String where = TransactionDBOpenHelper.TRANSACTION_DELETED + " != 1";
        getTransactions(where,null);
        return transactions;
    }

    private void getTransactions(String where, String[] whereArgs) {
        transactions = new ArrayList<>();
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        String[] kolone = new String[]{
                TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID,
                TransactionDBOpenHelper.TRANSACTION_ID,
                TransactionDBOpenHelper.TRANSACTION_TITLE,
                TransactionDBOpenHelper.TRANSACTION_AMOUNT,
                TransactionDBOpenHelper.TRANSACTION_DATE,
                TransactionDBOpenHelper.TRANSACTION_DESCRIPTION,
                TransactionDBOpenHelper.TRANSACTION_INTERVAL,
                TransactionDBOpenHelper.TRANSACTION_END_DATE,
                TransactionDBOpenHelper.TRANSACTION_TYPE,
                TransactionDBOpenHelper.TRANSACTION_DELETED
        };

        Uri adresa = Uri.parse("content://rma.provider.transactions/elements");
        String order = null;
        Cursor cursor = cr.query(adresa,kolone,where,whereArgs,order);
        if(cursor!=null && cursor.getCount()>0){
            cursor.moveToFirst();
            Transaction t = null;
            do {
                t= new Transaction();
                int intIdPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERNAL_ID);
                int idPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_ID);
                int titlePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TITLE);
                int amountPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_AMOUNT);
                int datePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DATE);
                int descPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DESCRIPTION);
                int intPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_INTERVAL);
                int endDPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_END_DATE);
                int typePos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_TYPE);
                int delPos = cursor.getColumnIndexOrThrow(TransactionDBOpenHelper.TRANSACTION_DELETED);
                t.setInternalId(cursor.getInt(intIdPos));
                t.setId(cursor.getInt(idPos));
                Type tip = getType(cursor.getInt(typePos));
                t.setType(tip);
                t.setTitle(cursor.getString(titlePos));
                t.setAmount(cursor.getDouble(amountPos));
                t.setDeleted(cursor.getInt(delPos));
                try {
                    t.setDate(df.parse(cursor.getString(datePos)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                    t.setItemDescription(cursor.getString(descPos));
                    t.setTransactionInterval(cursor.getInt(intPos));
                    if(cursor.getString(endDPos)!=null)
                        t.setEndDate(df.parse(cursor.getString(endDPos)).toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
                    else t.setEndDate(null);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                transactions.add(t);
            }while(cursor.moveToNext());
            cursor.close();
        }

    }

    private void getRegular(Transaction t, int month, int year) {
        Calendar ca = Calendar.getInstance();
        ca.set(Calendar.YEAR, t.getDate().getYear());
        ca.set(Calendar.MONTH, t.getDate().getMonthValue() - 1);
        ca.set(Calendar.DAY_OF_MONTH, t.getDate().getDayOfMonth());
        Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, t.getEndDate().getYear());
        c.set(Calendar.MONTH, t.getEndDate().getMonthValue() - 1);
        c.set(Calendar.DAY_OF_MONTH, t.getEndDate().getDayOfMonth());
        ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
        while (!ca.after(c) ) {
            if(year==0 || (ca.get(Calendar.MONTH)==month && ca.get(Calendar.YEAR)==year)){
                transactions.add(t);
            }
            ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
        }
    }

    public void deleteAll(){
        ContentResolver cr = context.getApplicationContext().getContentResolver();
        cr.delete(Uri.parse("content://rma.provider.transactions/elements"), null, null);
    }

    private Type getType(int id) {

        for (Type t : types) {
            if (t.getId() == id) return t;
        }
        return null;
    }
}
