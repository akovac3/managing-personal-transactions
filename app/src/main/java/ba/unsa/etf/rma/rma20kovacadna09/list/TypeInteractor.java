package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class TypeInteractor extends AsyncTask<Void, Void, Void> implements ITypesInteractor {
    private ArrayList<Type> types;
    private OnTypesSearchDone caller;

    public TypeInteractor(OnTypesSearchDone p) {
        caller = p;
        types = new ArrayList<Type>();
    };


    public interface OnTypesSearchDone {
        public void onTypesDone(ArrayList<Type> results);
    }

    @Override
    protected Void doInBackground(Void... voids) {

        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/transactionTypes";
        try{
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            int byteCharacter;
            String rezultat = "";
            while ((byteCharacter=in.read())!=-1){
                rezultat+=(char)byteCharacter;
            }
            JSONObject jo = new JSONObject(rezultat);
            JSONArray results = jo.getJSONArray("rows");
            for(int i =0;i<results.length();i++){
                JSONObject type = results.getJSONObject(i);
                String name = type.getString("name");
                int id = type.getInt("id");
                types.add(new Type(name, id));
            }
            urlConnection.disconnect();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onTypesDone(types);
    }

}
