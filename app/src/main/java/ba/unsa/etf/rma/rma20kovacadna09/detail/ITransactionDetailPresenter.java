package ba.unsa.etf.rma.rma20kovacadna09.detail;

import android.os.Parcelable;

import java.time.LocalDate;
import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public interface ITransactionDetailPresenter {

    void create(Integer internal, Integer id, LocalDate date, double amount, String title, Type type, String itemDescription, String transactionInterval, LocalDate endDate);
    Transaction getTransaction();
    ArrayList<Transaction> getTransactions();
    double getCurrentLimit(Transaction transaction, boolean all);
    void addTransaction(Transaction transaction);
    void removeTransaction(int id, int delete);
    void setTransaction(Parcelable transaction);
    void updateTransaction(Transaction nova);
    Transaction getTransactionById(int id);
}
