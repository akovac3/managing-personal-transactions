package ba.unsa.etf.rma.rma20kovacadna09.account;

import android.os.AsyncTask;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import ba.unsa.etf.rma.rma20kovacadna09.data.Account;

public class GetAccount  extends AsyncTask<String, Integer, Void> {
    private Account account;
    private OnAccountSearchDone caller;

    public GetAccount(OnAccountSearchDone caller) {
        this.caller = caller;
    }

    @Override
    protected Void doInBackground(String... strings) {
        String query = null;

        try {
            query = URLEncoder.encode(strings[0], "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String url1 = "http://rma20-app-rmaws.apps.us-west-1.starter.openshift-online.com/account/"+  query;
        try {
            URL url = new URL(url1);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String result = convertStreamToString(in);

            JSONObject jo = new JSONObject(result);
            int id = jo.getInt("id");
            double budget = jo.getDouble("budget");
            double totalLimit = jo.getDouble("totalLimit");
            double monthLimit = jo.getDouble("monthLimit");
            account = new Account(id, budget, totalLimit, monthLimit);

            in.close();
            urlConnection.disconnect();
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    @Override
    protected void onPostExecute(Void aVoid){
        super.onPostExecute(aVoid);
        caller.onDone(account);
    }


    public interface OnAccountSearchDone {
        public void onDone(Account results);
    }
}
