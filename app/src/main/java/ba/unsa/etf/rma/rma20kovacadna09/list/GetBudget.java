package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.ArrayList;
import java.util.Calendar;

import ba.unsa.etf.rma.rma20kovacadna09.R;
import ba.unsa.etf.rma.rma20kovacadna09.data.Transaction;
import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class GetBudget implements TransactionInteractor.OnTransactionsSearchDone, ITypeView {
    private ArrayList<Transaction> transactions;
    private Context context;
    private onBudgetDone caller;
    private ArrayList<Type> types = new ArrayList<>();
    private ITypesPresenter presenter;

    private ITypesPresenter getTypesPresenter() {
        if (presenter == null) {
            presenter = new TypePresenter(this, context);
        }
        return presenter;
    }

    @Override
    public void set() {

    }

    public interface onBudgetDone {
        public void onAmountDone(double amount);
    }

    public GetBudget(onBudgetDone caller, Context context) {
        this.context = context;
        this.caller = caller;

    }

    public boolean isConnected(){
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    public void changeBudget(){
        types = new ArrayList<>();
        types.addAll(getTypesPresenter().getTypes());
        String[] strings = {
                String.valueOf(0),
                context.getString(R.string.api_id)
        };
        if(isConnected())new TransactionInteractor(this, types).execute(strings);
        else{
            transactions = new TransactionDBInteractor(context, types).getAllTransactions();
            double amount = getCurrentBudget();
            caller.onAmountDone(amount);
        }
    }

    @Override
    public void onDone(ArrayList<Transaction> results) {
        transactions = results;
        double amount = getCurrentBudget();
        caller.onAmountDone(amount);
    }

    private double getCurrentBudget(){
        double result=0;
        Calendar ca = Calendar.getInstance();
        Calendar ed = Calendar.getInstance();
        for(Transaction t : transactions){
            if(t.getType().getName().equals("Individual income")) result+= t.getAmount();
            else if(t.getType().getName().equals("Individual payment") || t.getType().getName().equals("Purchase")) result -=t.getAmount();
            else {
                ca.set(Calendar.DAY_OF_MONTH, t.getDate().getDayOfMonth());
                ca.set(Calendar.MONTH, t.getDate().getMonthValue() - 1);
                ca.set(Calendar.YEAR, t.getDate().getYear());
                ed.set(Calendar.DAY_OF_MONTH, t.getEndDate().getDayOfMonth());
                ed.set(Calendar.MONTH, t.getEndDate().getMonthValue() - 1);
                ed.set(Calendar.YEAR, t.getEndDate().getYear());
                while(true) {
                    if (ca.after(ed)) break;

                    else {
                        if (t.getType().getName().equals("Regular income")) result += t.getAmount();
                        if (t.getType().getName().equals("Regular payment"))
                            result -= t.getAmount();
                    }
                    ca.add(Calendar.DAY_OF_MONTH, t.getTransactionInterval());
                }
            }
        }
        return  result;
    }
}
