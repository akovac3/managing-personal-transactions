package ba.unsa.etf.rma.rma20kovacadna09.list;

import android.content.Context;
import android.net.ConnectivityManager;

import java.util.ArrayList;

import ba.unsa.etf.rma.rma20kovacadna09.data.Type;

public class TypePresenter implements ITypesPresenter, TypeInteractor.OnTypesSearchDone {
    private ITypeView view;
    private Context context;
    private ArrayList<Type> types;
    private TypeModel  t;

    public TypePresenter(ITypeView view, Context context) {
        types = new ArrayList<>();
        this.view = view;
        this.context = context;
        t = new TypeModel();
    }
    private boolean isConnected(){
        return ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo() != null;
    }

    @Override
    public void searchTypes() {
        if(isConnected())
            new TypeInteractor((TypeInteractor.OnTypesSearchDone)this).execute();
        else{
            ArrayList<Type> results = new ArrayList<Type>() {
                {
                    add(new Type("Regular payment", 1));
                    add(new Type("Regular income", 2));
                    add(new Type("Purchase", 3));
                    add(new Type("Individual income", 4));
                    add(new Type("Individual payment", 5));
                }
            };
            t.napuniModel(results);
            types = results;
            view.set();
        }
    }

    @Override
    public ArrayList<Type> getTypes() {
        return t.getTypes();
    }

    @Override
    public void onTypesDone(ArrayList<Type> results) {
        t.napuniModel(results);
        types = results;
        view.set();
    }
}
